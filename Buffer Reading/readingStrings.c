// Reading output buffer from empset.com via CC3100 WiFi module.

// Shoutouts to Kevin Fernando for the advice and example code.


#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define MAX_MASS_STRING_LENGTH 6
#define MAX_TIME_STRING_LENGTH 6
#define MASS_OFFSET 8
#define TIME_OFFSET 6
#define MAX_FEED_TIMES 16


char *lookForTime(char *string, int *targetHour, int *targetMin);
char *lookForMass(char *string, int *targetMass);

int main(void) {
    char *myString = "Time: 00:30 OTHER CRAP Time: 00:45 Amount: 420 OTHER CRAP Time: 13:30 Amount: 200 OTHER CRAP OTHER CRAP" ;

    char *ret;

    int targetHour[MAX_FEED_TIMES];
    int targetMin[MAX_FEED_TIMES];
    int targetMass[MAX_FEED_TIMES];
    int presentHour;
    int presentMin;

    int feedTimes;

    ret = lookForTime(myString, &presentHour, &presentMin);

    printf("HOUR: %d MINUTE: %d \n", presentHour, presentMin);
    for (feedTimes = 0; feedTimes < MAX_FEED_TIMES; feedTimes++) {
        ret = lookForTime(ret, targetHour+feedTimes, targetMin+feedTimes); // muh pointer arithmetic
        if(ret == NULL) {
            break;
        }
        ret = lookForMass(ret, targetMass+feedTimes);
        printf("TIME[%d] = %d:%d\n", feedTimes, targetHour[feedTimes], targetMin[feedTimes]);
        printf("MASS[%d] = %d\n", feedTimes, targetMass[feedTimes]);
        if(ret == NULL) {
            break;
        }
    }

    printf("feedTimes %d\n", feedTimes); // this is the number of times the pets are being fed

    return 0;
}

char *lookForTime(char *string, int *targetHour, int *targetMin) { // will return ptr to after TIME
    
    if (string == NULL) {
        return NULL;
    }

    char *ret = strstr(string, "Time:");

    if (ret == NULL) {
        return NULL;
    }

    char *timeLoc = ret + TIME_OFFSET; // Now at start of time value - this position can be hard-coded.
    char timeValue[MAX_TIME_STRING_LENGTH]; 
    memcpy(timeValue, timeLoc, MAX_TIME_STRING_LENGTH); // Copy time value into new buffer
    timeValue[MAX_TIME_STRING_LENGTH-1] = '\0'; // null terminate buffer
    sscanf(timeValue, "%d:%d", targetHour, targetMin); // convert into integers.
    return timeLoc;
}

char *lookForMass(char *string, int *targetMass) {

    if (string == NULL) {
        return NULL;
    }

    char *ret = strstr(string, "Amount:");

    if (ret == NULL) {
        return NULL;
    } 

    char *amountLoc = ret + MASS_OFFSET; // Now at start of mass value - this position can be hard-coded.
    char mass[MAX_MASS_STRING_LENGTH]; 

    int i = 0;
    while(isdigit(*amountLoc)) { // loop to copy digit characters into msss array
        mass[i] = *amountLoc;
        i++;
        amountLoc++;
    }
    mass[i] = '\0';

    sscanf(mass, "%d", targetMass); // convert into integer.
    return amountLoc;
}