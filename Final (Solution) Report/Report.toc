\contentsline {section}{List of Figures}{3}{section*.2}
\contentsline {section}{\numberline {1}Introduction}{4}{section.1}
\contentsline {section}{\numberline {2}Explanation of Product and Design Concept}{5}{section.2}
\contentsline {section}{\numberline {3}Market analysis}{7}{section.3}
\contentsline {section}{\numberline {4}Detailed Design and Technical Specifications}{11}{section.4}
\contentsline {subsection}{\numberline {4.1}Principle of Operation}{11}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Subsystem Design}{11}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Microprocessor Subsystem Selection and Design}{12}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Internet Connectivity Subsystem Selection}{14}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}Quantity Detection Subsystem Design}{14}{subsubsection.4.2.3}
\contentsline {subsubsection}{\numberline {4.2.4}Dispenser Subsystem Design}{15}{subsubsection.4.2.4}
\contentsline {subsubsection}{\numberline {4.2.5}Power Subsystem Design}{16}{subsubsection.4.2.5}
\contentsline {subsubsection}{\numberline {4.2.6}Website Design}{17}{subsubsection.4.2.6}
\contentsline {subsection}{\numberline {4.3}PCB Design}{18}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Technical Specifications}{18}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Bill of Materials}{19}{subsection.4.5}
\contentsline {section}{\numberline {5}Manufacturing Cost Estimates}{21}{section.5}
\contentsline {section}{\numberline {6}Business Plan}{23}{section.6}
\contentsline {section}{\numberline {7}Development Plan}{24}{section.7}
\contentsline {section}{\numberline {8}Summary}{26}{section.8}
\contentsline {section}{\numberline {9}Bibliography}{27}{section.9}
\contentsline {section}{Appendix A: Requirements}{28}{section*.23}
\contentsline {section}{Appendix B: Survey}{28}{section*.24}
\contentsline {section}{Appendix C: CC3100-BOOST Bill of Materials}{37}{section*.25}
\contentsline {section}{Appendix D: PCB Schematic}{39}{section*.26}
\contentsline {section}{Appendix E: PCB Layout and Design}{40}{section*.27}
\contentsline {section}{Appendix F: Advisory Board Minutes}{40}{section*.28}
