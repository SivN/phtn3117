% WORD IS FOR PLEBS
\documentclass[12pt, oneside, a4paper]{article}
\usepackage{mathtools}
\usepackage[square,sort,comma,numbers]{natbib}
\usepackage{textcomp}
\usepackage{graphicx}
\usepackage{float}
\usepackage{caption}
\usepackage{parskip}
\usepackage{lscape}
\usepackage{pdfpages}
\usepackage[nottoc,numbib]{tocbibind}
\usepackage{rotating}
\usepackage{hyperref}

\numberwithin{figure}{section}
\numberwithin{table}{section}

\let\oldenumerate\enumerate
\renewcommand{\enumerate}{
  \oldenumerate
  \setlength{\itemsep}{1pt}
  \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt}
}


\begin{document}

\includepdf[pages=-]{Coversheet}

\title{ELEC3117 Product Development Proposal}
\date{}
\author{James Gray, z3459143 and Siavash Nasr, z3459157} 
% Supervised by Emeritus Professor John Felix Anthony Cena

\maketitle 

\renewcommand{\abstractname}{Executive Summary}
\begin{abstract}

\noindent 
Currently there are very few low-cost automatic pet feeding systems available to consumers, especially in the case of domestic birds. In this project we seek to design and develop an automated bird feeding system from which the user will be able to determine when and how much the bird will be fed from a web-based interface.

\bigskip
\noindent
In order to create a competitive product, we have incorporated a number of unique features into the design, most notably the versatility of its web interface, allowing it to be accessed from any device with internet browsing capabilities. This is achieved through the implementation of a micro-controller used in conjunction with a Wi-Fi based internet-connected subsystem, as well as mass sensors and motors.

\bigskip
\noindent
Currently there are 295350 \citep{6_abs.gov.au_2007} household who are looking to buy pet birds, and we have estimated that 14\% of these households  would be willing to purchase our product. At a sale price of \$100 and a manufacturing cost of \$80.93 we can potentially make a profit of \$607780.3 in the first year, which we predict to increase to \$777644.5 by the third year. This will yield a net present value of \$2043212.1. This indicates that there are opportunities to make a substantial profit selling this product.

\bigskip
\noindent
There are a number of risks associated with the development of this project, such as mistake being made during the development process, as well as issues involving integration of subsystems. Non-technical risks include logistical and time management issues. Steps will be taken to minimise these potential risks, in particular ensuring that thorough prototyping is performed.

\bigskip
\noindent
In order to complete this project we will require laboratory time, equipment and a number of electronic components.  We will also require raw materials for the manufacture of the physical enclosure of the device. 

\end{abstract}

\tableofcontents
\listoffigures

\newpage

\section{Introduction}

We intend to develop a smart bird feeder. This will be marketed towards pet-owners, vets, animal shelters and pet stores. It will be aimed at feeding birds dry food automatically. It will be controlled via a web-based interface and will provide alerts to the user via a web-based interface.

There are a number of products on the market that perform similar functions. The Petnet \textit{SmartFeeder} provides similar functionality, however it is aimed at dogs and cats \cite{1_petnet.io_2015}, not birds. It also only utilises an iPhone app interface, which is a substantial limitation as iPhone users only account for 45.1\% of the Australian smartphone market, 24.1\% of the European and 47.7\% of the US market \cite{kantar_wordpanel}. In addition, there is no interface for computers, for those who may not own or wish to use a smartphone.
 
Another smart pet feeder is the \textit{feedandgo}. This is also aimed at dogs and cats. While its web-interface works on all devices, it can only store food for up to six meals \cite{4_feed_and_go_2015}. This is a fundamental limitation this device, as it means that for users who are traveling for more than a maximum of six days cannot rely on the pet to be automatically fed.

There are multiple automatic pet feeders that do not have an internet interface such as the \textit{Pawever Automatic Pet Feeder} \cite{2_australia_2015}. This clearly lacks a method of remote control and can't provide alerts when your pet has been fed. 

This means the central problem for this project will be designing and building a device that can dispense food at times and in amounts specified by a web-based interface. It must also upload data to the website regarding when the bird has been fed and how much it has eaten.

This Project Development Proposal will contain several other sections. These are as follows:

\begin{itemize}
  \item Explanation of Product and Design Concept. This will contain information on the nature of the product concept, including, unique-selling points, expected use, requirements and the high level technical approach taken to develop the product prototype.
  \item Market Analysis. Here estimates are provided on the size of the market, market share, and appropriate price points, as well as analysis of primary and secondary research to support these estimates.
  \item Manufacturing Costs. This will contain estimates on the manufacturing costs and estimates of profit. 
  \item Business Plan. This section will contain the estimates on potential profitability.
  \item Development Plan section which will outline the plan we will pursue to developing the prototype smart pet feeder.
  \item Appendix. This will contain the full set of requirements for the product, market research survey responses, and bills of materials used in cost estimates. 
\end{itemize}

\section{Explanation of Product and Design Concept}

The operation of the bird feeder is designed to be as simple as possible - the user sets the time of day that the bird should be fed, and the amount that should be dispensed at that time. The feeder also alerts the user when the bird has been fed, and when the food dispenser requires refilling. This system requires very little interaction making it ideal for situations where constant physical presence is infeasible, such as when users travel for long periods often or when users own dozens of pets, for example in the case of a bird sanctuary or veterinarian clinic. 

In essence, it is a system designed to be as non-obtrusive as possible so that users are able to have peace of mind while they are in situations where it simply would not be possible to personally feed their pets.

The most important unique selling points of this product are the fact that the smart pet-feeder is aimed at feeding pet birds and that it has a web-based interface that can be accessed from all internet connected devices. % Pinched this bit from the intro

A successful prototype smart pet feeder will satisfy a number of requirements which are described in full in the appendix of this report. The key requirements are:

\begin{itemize}
  \item The device shall be controlled via a web-based interface
  \item The device shall dispense food at the time specified by the user and in the amount specified
  \item The device shall determine if there's no food left in its dispenser and alert the user if that is the case
  \item The device shall notify the user when the animal has eaten and how much it has eaten via the internet
\end{itemize}

The requirements of internet based remote control, and that of notifying the user when the animal has eaten or when the smart pet feeder has run out of food necessitates a two way connection to the internet. The requirement of controlling the amount of food to dispense and determining how much food is left to dispense requires methods of measuring quantities of food. Implementing dispensing food at the correct time requires some form of controlling the whole device and interfacing with a method of dispensing food.

This brief analysis of these key requirements means that our high level design will follow the architecture depicted in figure ~\ref{OverallArchitecture}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5, trim = 300 0 300 0]{OverallArchitecture} % needs slight modification add power for it
	\caption{\textit{\small{Block Diagram of all the subsystems in the design for this product.}}}
	\label{OverallArchitecture}
\end{figure}

The use of a Microcontroller (MCU), as the central controller of the product was selected on the basis that it enables us to easily interface with both digital and analogue peripherals, whilst also allowing software layer to exist. This allows it to execute complex routines, such as interfacing with the Internet Connectivity Subsystem, without the need for too much specific hardware.

Various options were considered for the Quantity Detection Subsystem. There are two different methods for detection of the amount of food, mass and volume. Mass was selected as it seemed much simpler, only requiring one sensor at the bottom of each container, as opposed to distributed sensors  or cameras, in the case of volume. For this job, force sensitive resistors, were selected, because, they are simple and relatively cheap.

The design approach towards the Internet Connectivity Subsystem was focused on Wi-Fi. This is for several reasons. Worldwide, as of 2014 with 65\% of broadband users \cite{5_smith_2014} adopting Wi-Fi, this product is compatible with most internet connected homes. In addition, the nature of the product, being in close proximity to animals that may chew on cables, lends itself to a wireless solution. While a mobile internet connection such as 3G could achieve this, this connection between the device and the mobile internet network would not be provided for free by the mobile phone network operators. This would either require consumers to pay an ongoing connectivity fee for the product, which would make it less marketable, or require the business to pay for it ourselves, increasing costs. % plz proofread this

Because most pet birds are kept indoors and internet connections typically necessitate an electricity connected home, the design decision for powering the product was fairly straightforward. A simple 5VDC wall adapter, followed by a linear regulator, to ensure the voltage is compatible with the different subsystems was selected. This was because it was a simple and reliable system. 

The Dispenser consists of the physical device and mechanism used to dispense the food. While the design of the physical device is not yet finalised, it has been decided that it shall have an animal-proof container that can hold 500mL of dry food, and will have a hatch that can open, allowing food to be dispensed into a bowl. It has been decided that the hatch will be open and closed by a motor, controlled by the MCU.

\section{Market analysis} 

The number of households that seek to become pet owners is around 1.1 million \citep{7_kb.rspca.org.au_2014}. In a 1994 survey by the Australian Bureau of Statistics \cite{6_abs.gov.au_2007}, it was estimated that 26.9\% of households owned birds as a pet, resulting in a market size of 295350 households.

In order to develop a good idea of how large the market for a smart pet feeder is, a sample of 50 pet owners was surveyed on a number of points relating to pet ownership and diet, the key points being the responder's pet feeding schedule, travel schedule, and concern towards the health of their pet with regards to diet. The survey and its results have been include in the appendix of this report.

Concerns across all pet owners are universal, meaning that what is reflected within the survey does not depend on any particular pet type. Additionally we note that this product can be easily scaled to feed other types of pets. 

We discovered that the majority of pet owners (73\%) feed their pets, on average, once or twice daily  as illustrated in the below figure ~\ref{FeedSchedule1}
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.50]{FeedSchedule1}
	\caption{\textit{\small{A breakdown of pet owner's feeding schedules.}}}
	\label{FeedSchedule1}
\end{figure}  

We also found that a reasonably large proportion of respondents, around 29\%, required their pets to be minded as often as every 3 to 6 months. This suggests that there certainly are individuals who would benefit from the use of a smart bird feeder due to their frequency of travel. A further breakdown of travel habits can be seen in the figure ~\ref{TravelHabits1} below.
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.50]{TravelHabits1}
	\caption{\textit{\small{A breakdown of pet owner's travel habits.}}}
	\label{TravelHabits1}
\end{figure}

Another important consideration that was investigated through the survey was pricing. We determined that of the pet owners who were interested in the product, around 14\% were willing to spend over 100 dollars on an automatic bird feeder as illustrated in figure ~\ref{CostSelection1} below. This means, based on our market size, there is a possibility of selling up to 41349 units in the first year, if the price point is at \$100.
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.50]{CostSelection1}
	\caption{\textit{\small{Detailed look at pricing.}}}
	\label{CostSelection1}
\end{figure}

Finally, the survey responses indicated that the vast majority of pet owners were concerned about their pets receiving the correct amount of food at feeding time. Again, this is a strength of the smart bird feeder, which allows pet owners to specify the amount of food that will be dispensed for the bird to eat. The below figure ~\ref{DietImp1} illustrates the overall make up of responses with respect to pet diet.
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.50]{DietImp1}
	\caption{\textit{\small{Pet owner attitudes toward pet diet.}}}
	\label{DietImp1}
\end{figure}

\section{Manufacturing Cost Estimates}

An estimate for the per unit cost was performed. We began by estimating the costs of the components of each subsystem, then estimating the cost for PCB manufacturing, handling, assembly and shipping. A margin of 20\% was then added to the product to account for overheads. Personnel costs were derived by assuming two engineers would be actively developing the product for 8 hours per week, over 12 weeks, at a rate of \$120 per hour.

It was noted that this stage, there are likely to be many similarities between the MSP430F5529LP development kit, the CC3100BOOST wifi-module and electronics of our smart pet feeder. These are currently what is being used to prototype the software. By checking the bill of materials (BOM) of these products, we can arrive, at a fairly precise estimate of the cost of components for the electronics of this project. The bill of materials can be found in Appendix C.

The MSP430F5529LP development contains an onboard programmer and debugger. While this is necessary for programming the chip, we will not need this for the final product. Thus, these parts of the BOM can be neglected in this estimate. This leaves the components shown in table \ref{table: MSP430_COST}. Their costs are estimated based on their bulk or on estimates contained in the lecture notes \cite{1_taubman_2015}. 

\begin{table}[H]
\begin{center} \caption{MCU Components Costs}  \label{table: MSP430_COST}
  \begin{tabular}{ | l | c | r |}
    \hline
    Quantity & Component & Unit Cost (\$) \\ \hline
    12 & Capacitors & 0.02 \\ \hline
    9 & Resistors & 0.01 \\ \hline
    1 & MSP430F5529 & 5.48 \\ \hline
    1 & Crystal Oscillator & 0.35 \\ \hline
    29 & Header Pins & 0.213 \\ \hline
     & \textbf{Total} &  12.33 \\ 
    \hline
  \end{tabular}
\end{center}
\end{table}

The costs of the components in the Wi-Fi module are shown in the table \ref{table: WIFI_COST}. These were calculated in the same manner as the components in table \ref{table: MSP430_COST}.
\begin{table}[H]
\begin{center} \caption{Internet Subsystem Components Costs}  \label{table: WIFI_COST}
  \begin{tabular}{ | l | c | r |}
    \hline
    Quantity & Component & Unit Cost (\$) \\ \hline
    30 & Capacitors & 0.02 \\ \hline
    41 & Resistors & 0.01 \\ \hline
    1 & CC3100 & 11.66 \\ \hline
    2 & Crystal Oscillators & 0.30 \\ \hline
    20 & Header Pins & 0.213 \\ \hline
    4 & Wire wound inductors & 0.08 \\ \hline
    2 & Ferrite Beads & 0.02 \\ \hline
    1 & RF Switch & 0.20 \\ \hline
    1 & RF Coaxial Connector & 0.4 \\ \hline
    1 & RF Filter & 0.19 \\ \hline
    2 & Diodes & 0.09 \\ \hline
    2 & Mosfets & 0.03 \\ \hline
    1 & LDO Linear Regulator & 1.23 \\ \hline
    1 & Antenna & 0.55 \\ \hline
     & \textbf{Total} &  21.11 \\ 
    \hline
  \end{tabular}
\end{center}
\end{table}

This is therefore an estimate of the internet connectivity and MCU subsystem components shown in fig \ref{OverallArchitecture}. The cost of the quantity sensing components is estimated to be around \$12. This is because mass sensors are cost around \$3 each assuming bulk purchase of these parts, whilst allowing for some external circuitry. We will need two of these sensors, because it is necessary to determine the amount left in the dispenser and the amount being delivered to the bird. %cite for this $3 claim.

We estimate that the material and manufacturing costs for the physical device cost under \$5. %not 100% sure on this...

A motor is also necessary to open and close a hatch, to dispense food. It is estimated this will cost \$1.

We estimate the cost of PCB manufacturing to be less than \$7 per unit. This is based on a figure of \$2 per PCB, and \$0.01 spent soldering each component lead (we believe that we will require less than 500 component leads). This is based on the lecture notes \cite{1_taubman_2015}. %need a cite

We then estimate that shipping will cost \$2 per unit \cite{1_taubman_2015}. 

This adds up to a total of \$67.44 per unit. Using a 20\% margin for overheads, we arrive at our final estimate of \textbf{\$80.93}. 

\section{Business Plan}
% Need to add purpose, strategic plan, benefits

% Table based of Alex's example in lectures. Assuming a sales price of $100 - upper limit of what most people are willing to pay.
% Q2, Q6, Q10 have higher sales numbers due to the inclusion of Christmas/Boxing Day during this time.
% Numbers pulled from my arse.

From the market research analysis, we determined that there is the possibility of selling up to 4198660 units by the end of the third year of business. In the below table ~\ref{table: BUSINESS_PLAN}, we have outlined a business plan looking forward three years, selling the product at \$100. This price point was selected as it was the lower limit of what the market is willing to pay whilst still being profitable, based on responses from the market research. It is assumed that we will not have full market penetration in the first year, and as such the sales figures have been adjusted to be 20\% smaller than what was initially estimated.

\begin{table}[H]
\begin{center} \caption{Projected business plan}  \label{table: BUSINESS_PLAN}
  \begin{tabular}{ | l |*{3}{c}|}
    \hline
    & Year 1 & Year 2 & Year 3\\ \hline
    Development costs (\$) & 23040 & 23040 & 23040\\ \hline
    Sales (units) & 33079.2 & 41406.9 & 41986.6\\ \hline % 20% fewer units in first year
    Sales revenue (\$) & 3307920 & 4140690 & 4198660\\ \hline
    Manufacturing costs(\$) & 2677099.7 & 3351060.4 & 3397975.5\\ \hline
    Operating profit(\$) & 607780.3 & 766589.6 & 777644.5\\ \hline
    Present Value (\$) & 607780.3 & 730085.3 & 705346.5\\ \hline
  \end{tabular}
\end{center}
\end{table}

From table ~\ref{table: BUSINESS_PLAN}, our net present value after three years was determined to be \$2043212.1, indicating a strong potential profitability.

The projected growth in sales was calculated by comparing the proportion of the Australian population who are planning to purchase a pet in the future \cite{7_kb.rspca.org.au_2014} (1.1 million) against the annual growth in the Australian population, which as of 2013 is 1.4\%. \cite{8_abs.gov.au_2014}. 

\section{Development Plan}

Key resources for this product include access to lab time and equipment, time to work on the product outside of labs, the electronic components and the physical materials we require to construct the physical device. We intend to spend all the lab hours working towards developing the final product. We also intend to spend at least three hours per week outside of the labs, working on either the final product, or on other aspects of the project such as documentation. We have already purchased, or have access to the majority of electrical components needed. In order to build the physical enclosure, we intend to either re-purpose materials from other sources or purchase them for as little as possible.

The plan for developing this product follows the Gantt Chart shown in figure ~\ref{GANTT}. All the tasks will be worked on by both James Gray and Siavash Nasr.
\begin{landscape}
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{GanttChart} 
	\caption{\textit{\small{A Gantt Chart depicting the planned workflow of the project}}}
	\label{GANTT}
\end{figure}
\end{landscape}

For the weeks up to and including Week 6, this semester, we have so far:
\begin{itemize}
	\item Sourced and Ordered Components
	\item Sourced Critical Reference Designs
	\item Written Software Drivers
	\item Conducted market research through an online survey and drawn conclusions based on the survey.
\end{itemize}

In Week 7, we intend to construct and test a prototype circuit, using breadboards and development kits. In addition we will design and order a printed circuit board (PCB) or boards to replace the development kits.

In Week 8, we will begin interfacing with the software with the prototype circuits we have built.

In Week 9, we will write a comprehensive program that performs all the functions required of the device.

In the Midsemester Break, we will construct our PCB, and build our physical device, whilst finalising testing for the prototype circuits.

In Week 10, we will test our PCB, to ensure it functions appropriately. 

In Week 11, we will perform further testing on our PCB, to ensure it integrates with the software.

The following week, Week 12, we will finalise our product, and prepare for the seminar and begin writing the final report. 

In Week 13, we will write the Final Report.

There are number of key risks associated with the development of this product. Technical risks include:	\begin{enumerate}
	\item Components not working as expected.
	\item Difficulties in integration as components may behave differently to expected when operating with other components.
	\item Mistakes being made during PCB Design, which may only be detected after the PCB arrives. 
\end{enumerate}

In terms of technical risks, we believe that risk 3 will is the most concerning, because of the substantial lead time in ordering PCB. We will attempt to minimise the risks 1 and 2 by performing extensive prototyping before building the final product. In the case of risk 3, we will conduct an internal design review before ordering the PCB.

There are a number of project management risks as well. These include:

\begin{enumerate}
	\item Components being shipped late, or faulty and hence delaying the project.
	\item Other time commitments causing members to fall behind on the project.
	\item A failure to complete tasks on the critical path on time, as per the Gantt Chart. 
\end{enumerate}

Ordering components relatively early in the semester, which we have already done, will minimise risk 1. The other risks can be decreased by ensuring that both team members employ good time management. 

\section{Summary}
In conclusion, it was determined that the smart bird feeder has the potential to be a profitable venture with a number of key unique features that set it apart from the other products that are currently available.

Our unique selling points primarily revolve around the use of the web-based interface which provides the potential to be accessed by many different devices, furthermore it is the only device available that focusses primarily on pet birds. This sets the product apart from other automated pet feeding systems, which are mainly marketed to larger domestic pets such as cats or dogs.

The device will consist of a number of key subsystems involving:
\begin{itemize}
	\item a power subsystem
	\item a microcontroller
	\item an internet connectivity subsystem
	\item a dispenser containing a motor-controlled hatch
	\item sensor systems
\end{itemize}

We have estimated that the device will cost \$80.93 per unit, and will be able to be sold for \$100 per unit, resulting in a profit of \$19 per unit. We predict that we will be able to sell 116473 units over three years, resulting in a net present value of \$2043212.1.

There are a number of risks, both technical and non-technical, involved with the development of this product. These include integration issues and mistakes being made during development as technical risks, and poor time-management and logistical issues as non-technical risks. However, steps are being taken in order to mitigate these issues, such as extensive prototyping and placing component orders as early as possible.

\bibliographystyle{ieeetr}

\cleardoublepage

\phantomsection


\addcontentsline{toc}{section}{References}
\bibliography{Report}

\newpage

\appendix

\addcontentsline{toc}{section}{Appendix A: Requirements}
\section*{Appendix A: Requirements}


\textbf{Power Requirements:
}
\begin{enumerate}
  \item The device shall run on a 5V DC bus
  \item The device shall not draw more than 2A of peak current
\end{enumerate}

\textbf{Connectivity Requirements:}

\begin{enumerate}
  \item The device shall connect to the internet
  \item The device shall receive feeding times, and amounts from a website
  \item The product shall update the website when the pet has been fed or when the dispenser has run out of food
\end{enumerate}

\textbf{Physical Requirements:
}
\begin{enumerate}
  \item The device shall dispense food when specified and in the amount specified by the user  
  \item The device shall not allow the pet to access un-dispensed food
  \item The device shall measure the amount of food dispensed and the amount of food left to be dispensed
  \item The device shall hold more than 500mL of food
  \item the device shall not prematurely spoil the food it contains 
  \item the device shall be able to be mounted on a bird cage or enclosure
\end{enumerate}


\textbf{Safety and Environmental Requirements:
}
\begin{enumerate}
  \item The device shall not pose any health or safety risks to humans or animals
  \item The device shall not make sufficient noise to spook animals
\end{enumerate}

\addcontentsline{toc}{section}{Appendix B: Survey}
\section*{Appendix B: Survey}
\begin{itemize}
	\item How many pets do you own?
	\item What kind of pets do you own?
	\item How often do you feed your pets?
	\item If there was a product to feed your pets automatically, how much would you be willing to pay for it? 
	\begin{itemize}
		\item I wouldn't buy it at all
		\item \$10 - \$20
		\item \$20 - \$50
		\item \$50 - \$100
		\item \$100 - \$250
		\item \$250 - \$500
		\item \$500+
	\end{itemize}
	\item On average how often do you require others (other than your family or roommates) to mind your pets because of travel? 
	\begin{itemize}
		\item Once per month or more frequently
		\item Once every 1-3 months
		\item Once every 3-6 months
		\item Once every 6-12 months
		\item  Once every 1 year or less 
	\end{itemize}
	\item Pick the statement that you feel is most applicable to you: 
	\begin{itemize}
		\item My pet is on a strict diet
		\item I generally try to feed my pet roughly the right amount
		\item I am not very concerned about my pets over or undereating
		\item I allow my pet to eat as much as it wants
		\item  I do not own a pet
		\item Other: 
	\end{itemize}
	\item How important do you believe a pet's diet is to its health?
	\begin{itemize}
		\item Extremely important
		\item Highly important
		\item Moderately important
		\item Not very important at all
		\item Not important at all
		\item Other:
	\end{itemize}
	\item Is pet obesity a concern to you?
	\begin{itemize}
		\item Pet obesity is not a concern to me
		\item I am moderately concerned about pet obesity
		\item I am very concerned about pet obesity
		\item Other: 
	\end{itemize}
\end{itemize}

The results of the survey are as follows:
		\includepdf[pages=-]{surveyresponsesedit}
\addcontentsline{toc}{section}{Appendix C: Bill of Materials}
\section*{Appendix C: Bill of Materials}
The following pages contain the bill of materials for the micro-controller unit and Wi-Fi module respectively.
\includepdf[pages=-]{mcubom2}
\includepdf[pages=-]{wifibom}

\end{document}


