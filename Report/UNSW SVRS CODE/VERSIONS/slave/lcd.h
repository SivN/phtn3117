#ifndef _LCD_H_
#define _LCD_H_

void LCDInit();
void LCDMove(uint8_t row, uint8_t col); // Moves where you're writing to to these coordinates
void LCDString(char *str); // print a null terminated string to LCD
void LCDHex8(uint8_t val); // print 8 bit number to LCD
void LCDHex16(uint16_t val); // print 16 bit number to LCD
void LCDHex32(uint32_t val); // print 32 bit number to LCD

#endif /* _LCD_H_ */

