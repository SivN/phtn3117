#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include "softuart.h"
#include <string.h>
#include <util/crc16.h>
#include "main.h"
#include "lcd.h"

// this global variable stores the current time in ms
static volatile uint16_t time_irq;
volatile uint16_t fast_time;
extern volatile char   inbuf[SOFTUART_IN_BUF_SIZE];
extern volatile unsigned char  flag_tx_busy;

// Puts the CPU into sleep mode.
void __attribute__ ((__noinline__)) go_to_sleep(uint8_t mode)
{
      set_sleep_mode(mode);
      cli();
      {
        sleep_enable();
        sei();
        sleep_cpu();
        sleep_disable();
      }
      sei();
} // Wakes up on any interrupt.


int main(void)
{
  uint8_t i;
  uint16_t check;
  char tx_string[TX_LENGTH] = {MY_ADDRESS, MY_NEXT_ADDR, 0x30, 0x30, 0x30, 0x30, 0, 0}; // send arbitary data
  uint16_t last_time = 0;


  softuart_init();
  softuart_turn_rx_on(); /* redundant - on by default */
  setupWDT();
  setupPWM();

  // set up LCD
  LCDInit();

  sei(); // global interrupt enable
  for(;;) 
  { // SELECT HIGH SPEED PERIPHERAL CLOCK AS PCK. THIS LETS YOU USE PWM PROPERLY
    PLLCSR &= ~(1<<LSM);
    PLLCSR |= (1 << PLLE) | (1<< PCKE);
 
    cli(); // global interrupt disable
    uint16_t time = time_irq; // read time_irq atomically
    sei(); // global interrupt enable
    softuart_flush_input_buffer();
    if(last_time < time) {
      check = 0; // calculate checksum and place it into the CHECKSUM_BYTES
      for (i = 0; i < TX_LENGTH - 2; i++) {
        check = _crc_xmodem_update(check, tx_string[i]);
      }
      tx_string[CHECKSUM_BYTE-1] = check >> 8;
      tx_string[CHECKSUM_BYTE] = check & 0xFF;

      for(i = 0; i <TX_LENGTH; i++) { // Use loop as string is not null terminated
        softuart_putchar(tx_string[i]);
      }
      for (i = 0; i < TX_LENGTH; i++) {
        if(i < 5) {
          LCDMove(0, i * 3);
        } else {
          LCDMove(1, (i - 5) * 3);
        }
        LCDHex8(tx_string[i]);
      }
      //LCDMove(2,0);
      //LCDString("Count");
      //LCDHex8(time);
      last_time = time;
        for(i = 2; i < TX_LENGTH - 2; i++) {
        tx_string[i]++;
      } while(flag_tx_busy) {
        go_to_sleep(SLEEP_MODE_IDLE);
      } // need little delay loop
    //go_to_sleep(SLEEP_MODE_PWR_DOWN); 
    }
  }

  cli(); // global interrupt disable

  return 0; // never supposed to reach here.
}


ISR(WDT_vect)
{
  WDTCR |= (1 << WDIE); // must be done otherwise MCU will reset on the next WDT overflow
  time_irq++;           // this is despite the fact that we've already set this bit in setupWDT
}

void setupWDT() { // aim for 1 s
  WDTCR |= (1 << WDIE) | (1 << WDP2)| (1 << WDP1);
}
// WDT overflows once per second currently
/*void setupPWM(void) { // places a number of values in key registers.
  // read the datasheet to see which ones do what.
  PLLCSR &= ~(1<<LSM);
  PLLCSR |= (1 << PLLE) | (1<< PCKE); // set up PCK
  TCCR1 |=  (1<<CS12);
  GTCCR |= (1<<PWM1B) | (1 << COM1B0);
  OCR1B = PWM_OFF;
  OCR1C = PWM_MATCH_SIZE;
  TCNT1 = 0;
} */

void setupPWM(void) { //
  PLLCSR &= ~(1<<LSM);
  PLLCSR |= (1 << PLLE) | (1<< PCKE);
  TCCR1 = (1<<CS12);
  GTCCR |= (1<<PWM1B) | (1 << COM1B0);
  OCR1B = PWM_OFF;
  OCR1C = PWM_MATCH_SIZE;
  TCNT1 = 0;
  //cli();
  //TIMSK |= (1 << TOIE1);

}

void pwmOn() { // with post setup in mind
  OCR1B = PWM_ON;
}

void pwmOff() {
  OCR1B = PWM_OFF;
}

void setupPinInterrupts (void) {
    GIMSK |= 1 << PCIE;
    PCMSK |= 1 << PCINT4;
}
