#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include "main.h"
#include "lcd.h"
#include "pid.h"

// this global variable stores the current time in ms
/*static volatile uint16_t time_irq;

// this global variable stores the latest temp reading
static volatile uint16_t temp_irq;
static volatile uint8_t time_wdt = 0, delta_wdt;
static volatile uint8_t captured = 0;
static volatile uint16_t adc_captured = 0, temp = 0;*/

// Puts the CPU into sleep mode.
void go_to_sleep(uint8_t mode)
{
      set_sleep_mode(mode);
      cli();
      {
        sleep_enable();
        sei();
        sleep_cpu();
        sleep_disable();
      }
      sei();
} // Wakes up on any interrupt.

/*uint16_t myadc(void)
{

  ADCSRA |= _BV(ADIE);

  set_sleep_mode( SLEEP_MODE_ADC );
  sleep_enable();

  do
  {
    // The following line of code is only important on the second pass.  
    // For the first pass it has no effect.
    // Ensure interrupts are enabled before sleeping
    sei();
    // Sleep (MUST be called immediately after sei)
    sleep_cpu();
    // Checking the conversion status has to be done with interrupts 
    // disabled to avoid a race condition
    // Disable interrupts so the while below is performed without interruption
    cli();
  }
  // Conversion finished?  If not, loop.
  while( ( (ADCSRA & (1<<ADSC)) != 0 ) );

  sleep_disable();
  sei();

  ADCSRA &= ~ _BV( ADIE );
  return (ADC);
} */

//static uint8_t heat_counter = 0;

int main(void)
{
  //HEATER_PORT &= ~HEATER; // force heater output low
  //HEATER_DDR |= HEATER; // set heater pin as output

  SWITCH_PORT &= ~SWITCH; // force switch output low
  SWITCH_DDR |= SWITCH; // set switch pin as output

  //WDT_off();
  // You need to either turn off the WDT or configure it to not reset the processor on overflow
  //setupWDT();
  setupPWM();
  //go_to_sleep(SLEEP_MODE_PWR_DOWN);


  for(;;) // loop forever at 10Hz
  {  }

  return 0; // never supposed to reach here.
}


// ADC conversion interrupt service routine
/*ISR(ADC_vect)
{
  // store the latest temp reading in temp_irq
 // temp_irq = ADCL | (ADCH << 8);
} */

/*ISR(WDT_vect) // DUNNO WHAT THIS DOES, BETTER ASK.
{
  WDTCR |= (1 << WDIE);
  time_irq++;
}*/

/*void setupWDT() { // aim for 1 s
  WDTCR |= (1 << WDIE) | (1 << WDP2)| (1 << WDP1);
  // what about those safety levels though?
}*/

void WDT_off(void)
{
//_WDR();
/* Clear WDRF in MCUSR */
MCUSR = 0x00;
/* Write logical one to WDCE and WDE */
WDTCR |= (1<<WDCE) | (1<<WDE);
/* Turn off WDT */
WDTCR = 0x00;
}

/*void savePower() { // turns off timer0 (used for uart), USI and ADC
  power_timer0_disable();
  power_adc_disable();
  power_usi_disable();
}*/

/*void powerOn() { // turns on timer0, USI and ADC
  power_timer0_enable();
  power_adc_enable();
  power_usi_enable();
}*/

/*void delay(int ms) { // delay function enter the number of milliseconds here
  uint16_t start_time = time_irq;
  while (time_irq < start_time + ms){
  } // largely useless if you need to do accurate timing. For longer term things though it's okay.
} */

/*  // set up timer to be okay


  // set up TIMER1 for 0.1s ticks
  TCNT1 = 0; // set initial timer value
  OCR1A = 48; 
  //TCCR1 = (1 << CTC1);
  TCCR1 |=  (1 << CS10)   | (1 << CS11) | (1 << CS12) |(1 << CS13) ;
	TIMSK |= (1 << OCIE1A);
  
} */

/*void setupTimer0() {
  TCNT0 = 0; // set initial timer value
  OCR0A = 48; // set match register value
  TCCR0A |= (1<<WGM01); // set clear timer on compare match mode
  TCCR0B |= (1<<CS01) | (1<<CS00); // start timer, 64X prescaler
  TIMSK |= (1<<OCIE0A); // enable compare match interrupt
} */

void setupPWM(void) { //
  PLLCSR &= ~(1<<LSM);
  PLLCSR |= (1 << PLLE) | (1<< PCKE);
  TCCR1 = (1<<CS12);
  GTCCR |= (1<<PWM1B) | (1 << COM1B0);
  OCR1B = 100;
  OCR1C = 214;
  TCNT1 = 0;
  //cli();
  //TIMSK |= (1 << TOIE1);

}

/*void toggleLED(void) {
	SWITCH_PORT ^= SWITCH;
}*/

/*ISR(TIM1_OVF_vect) // TIMER1 INTERRUPT HANDLER
{
  //TCNT1 = 0; // gotta reset timer for some reason, I don't know why.
  //SWITCH_PORT ^= SWITCH;
  //time_irq++;
} */

/*ISR(TIM0_COMPA_vect)
{
  // count milliseconds in time_irq variable
  time_irq++;
}*/
