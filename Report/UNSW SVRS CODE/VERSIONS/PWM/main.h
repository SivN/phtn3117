#ifndef _MAIN_H
#define _MAIN_H

#define HEATER_PORT PORTB
#define HEATER_DDR DDRB
#define HEATER (1<<PB3)

#define SWITCH_PORT PORTB
#define SWITCH_DDR DDRB
#define SWITCH (1<<PB3)

#define SERIAL_PORT PORTB
#define SERIAL_DDR DDRB
#define SERIAL_USCK (1 << PB2)
#define SERIAL_DO (1 << PB1)
#define SERIAL_DI (1 << PB0)

#define PWM_MATCH_SIZE  26
#define PWM_OFF             26 // should be 0 unless using inverted PWM output, then use PWM_MATCH_SIZE.
#define PWM_ON              (1 * PWM_MATCH_SIZE / 2)

#define BITS 8

void setupTimer1(void);
void sendNumber (uint8_t byte);
uint8_t sendSerial(uint8_t byte);
void toggleLED(void);
void delay(int ms);
void savePower(void);
void powerOn(void);
void setupWDT(void);
void setupTimer0(void);
void setupPWM(void);
void WDT_off(void);
#endif /* _MAIN_H */

