#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>

#define TX_MASK ((1<<0) | (1 << 1))
#define MATCH_VAL 140  // work this out later may not be quite right

static volatile uint16_t tx_buffer; // but only use 14 bits of this
static volatile uint8_t bit_flag; // signifies if we're halfway through bit

void setupRC5() {
  TCNT0 = 0; // set initial timer value
  OCR0A = MATCH_VAL; // set match register value
  TCCR0A |= (1<<WGM01); // set clear timer on compare match mode
  TCCR0B |= (1<<CS01) | (1<<CS00); // start timer, 64X prescaler
  TIMSK |= (1<<OCIE0A); // enable compare match interrupt
}

// SEND LSB FIRST
void send_data(uint16_t to_send) { // TWO LSB are reserved
	tx_buffer = to_send;
	tx_buffer |= TX_MASK;
}

ISR(TIM0_COMPA_vect) {
	// read tx_buffer 
	// if bit_flag == 0
	// if it's a 1, turn pwm off
	// if it's a 0 turn pwm on
	// then set bit_flag to 1
	// if bit_flag == 1
	// toggle PWM and shift through tx_buffer by 1
}