PROJECT = coldstart
MODULES = main lcd pid softuart
SOURCES = ${MODULES:%=%.c}
HEADERS = ${MODULES:%=%.h}

#AVRDUDE_PROG    = avrisp2
AVRDUDE_PROG    = stk500
#AVRDUDE_PORT    = usb
AVRDUDE_PORT    = /dev/ttyUSB0
AVRDUDE_PART    = t25

# 0xe2 = 1110 0010, clock divided by 8, clock out enabled, max start-up time,
#                   internal 8MHz. 
LFUSE           = 0xe2

# 0xe2 = 1110 0010, clock divided by 8, clock out enabled, max start-up time,
#                   internal 125KHz. 
#LFUSE           = 0xe4


# 0xdc = 1101 1100 = defaults, with brown out level set to 4.3V
HFUSE           = 0xdc
EFUSE           = 0xff

CC              = avr-gcc
OBJCOPY         = avr-objcopy
OBJDUMP         = avr-objdump
SIZE            = avr-size

#CFLAGS          = -std=gnu99 -Wall -Wextra -Werror -Os -mmcu=attiny25
LDFLAGS         = -Wl,-Map,$(PRG).map -lm
#CFLAGS          = -std=gnu99 -Wall -Wextra -Werror -Os -mmcu=attiny25 -funsigned-char -funsigned-bitfields -DNDEBUG -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums
CFLAGS          = -std=gnu99 -Wall -Wextra -Os -mmcu=attiny25 -funsigned-char -funsigned-bitfields -DNDEBUG -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums

CFLAGS += -DF_CPU=8000000UL
CFLAGS += -DSERIAL_NUM=0x00000001UL
CFLAGS += -DFW_VERSION=0x11120800UL
CFLAGS += -DUSART_BAUD_RATE=57600
CFLAGS += -DMODBUS_ADDRESS=123

all: $(PROJECT).elf $(PROJECT).lst $(PROJECT).hex $(PROJECT)_eeprom.hex
	$(SIZE) $(PROJECT).elf

prog: $(PROJECT).elf $(PROJECT).lst $(PROJECT).hex $(PROJECT)_eeprom.hex
	avrdude -i1000 -c$(AVRDUDE_PROG) -P$(AVRDUDE_PORT) -p$(AVRDUDE_PART) -Uflash:w:$(PROJECT).hex -Ulfuse:w:$(LFUSE):m -Uhfuse:w:$(HFUSE):m -Uefuse:w:$(EFUSE):m -e -u -v



clean:
	rm -rf *.o *.elf *.map *.lst *.hex *.srec *.bin

%.elf: $(SOURCES) $(HEADERS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(SOURCES)

%.lst: %.elf
	$(OBJDUMP) -h -S $< > $@

%_eeprom.hex: %.elf
	$(OBJCOPY) -j .eeprom --change-section-lma .eeprom=0 -O ihex $< $@

%_eeprom.srec: %.elf
	$(OBJCOPY) -j .eeprom --change-section-lma .eeprom=0 -O srec $< $@

%_eeprom.bin: %.elf
	$(OBJCOPY) -j .eeprom --change-section-lma .eeprom=0 -O binary $< $@

%.hex: %.elf
	$(OBJCOPY) -j .text -j .data -O ihex $< $@

%.srec: %.elf
	$(OBJCOPY) -j .text -j .data -O srec $< $@

%.bin: %.elf
	$(OBJCOPY) -j .text -j .data -O binary $< $@

