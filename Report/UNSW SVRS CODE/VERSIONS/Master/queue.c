#include <stdlib.h>
#include <stdint.h>
#include <assert.h>

#include <avr/interrupt.h>

#include "queue.h"

/*queue_t *queue_create(uint8_t size)
{
  assert(size && !(size & (size - 1))); // size must be power-of-two

  queue_t *queue = (queue_t*)malloc(sizeof(queue_t));

  if(queue)
  {
    queue->data = (uint8_t*)malloc(sizeof(uint8_t)*size);
    queue->size = size;
    queue->head = 0;
    queue->tail = 0;
  }

  return queue;
}*/

/*void queue_destroy(queue_t *queue)
{
  if(queue)
    free(queue->data);

  free(queue);
}*/

uint8_t queue_dequeue(queue_t *queue, uint8_t *bytes, const uint8_t count)
{
  uint8_t dequeue_count = 0;

  while(dequeue_count < count && queue_length(queue))
  {
    cli();
    bytes[dequeue_count++] = queue->data[queue->tail++];
    queue->tail &= queue->size - 1;
    sei();
  }

  return dequeue_count;
}

uint8_t queue_enqueue(queue_t *queue, uint8_t *bytes, const uint8_t count)
{
  uint8_t enqueue_count = 0;

  while(enqueue_count < count && queue_space(queue))
  {
    cli();
    queue->data[queue->head++] = bytes[enqueue_count++];
    queue->head &= queue->size - 1;
    sei();
  }

  return enqueue_count;
}

void queue_init(queue_t *queue, uint8_t *data, uint8_t size)
{
  assert(size && !(size & (size - 1))); // size must be power-of-two

  queue->data = data;
  queue->size = size;
  queue->head = 0;
  queue->tail = 0;
}

