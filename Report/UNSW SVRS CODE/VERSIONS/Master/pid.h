#ifndef _PID_H_
#define _PID_H_

void PIDInit();
uint8_t PIDIterate(uint16_t temp);

#endif /* _PID_H_ */

