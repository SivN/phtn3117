#ifndef _MAIN_H
#define _MAIN_H

#define HEATER_PORT PORTB
#define HEATER_DDR DDRB
#define HEATER (1<<PB3)

#define SWITCH_PORT PORTB
#define SWITCH_DDR DDRB
#define SWITCH (1<<PB4)

#define SERIAL_PORT PORTB
#define SERIAL_DDR DDRB
#define SERIAL_USCK (1 << PB2)
#define SERIAL_DO (1 << PB1)
#define SERIAL_DI (1 << PB0)

#define ADDRESS_BYTE  0
#define NEXT_ADDR_BYTE 1
#define VOLT_BYTE	  3
#define TEMP_BYTE	  5
#define CHECKSUM_BYTE 7
#define TX_LENGTH     8

#define ACCEPT		  1
#define REJECT		  0
#define YES			  1
#define NO			  0

#define MY_ADDRESS	 0x10
#define MY_NEXT_ADDR 0x20

#define BITS 8

#define PWM_MATCH_SIZE  26
#define PWM_OFF             26 // should be 0 unless using inverted PWM output, then use PWM_MATCH_SIZE.
#define PWM_ON              (1 * PWM_MATCH_SIZE / 2)

void setupWDT(); // sets up WDT for 1s ticks and instead of a reset an interrupt is called
void setupPWM(void); // sets up PWM for a frequency of 38kHz and duty cycle of 50%

void go_to_sleep(uint8_t mode); // sends microprocessor to sleep

void setupPinInterrupts (void); // sets up pin interrupts on PB4

#endif /* _MAIN_H */

