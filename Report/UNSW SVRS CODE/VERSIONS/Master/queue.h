#ifndef _QUEUE_H_
#define _QUEUE_H_

#include <stdint.h>

typedef struct
{
  uint8_t *data;
  uint8_t size;
  uint8_t head;
  uint8_t tail;
} queue_t;

/* create a queue in dynamic memory */
//queue_t *queue_create(uint8_t size);

/* destroy a queue in dynamic memory */
//void queue_destroy(queue_t *queue);

/* dequeue one or more bytes */
uint8_t queue_dequeue(queue_t *queue, uint8_t *bytes, const uint8_t count);

/* enqueue one or more bytes */
uint8_t queue_enqueue(queue_t *queue, uint8_t *bytes, const uint8_t count);

/* initialise a queue in static memory */
void queue_init(queue_t *queue, uint8_t *data, uint8_t size);

/* returns the bytes length available to read */
static inline uint8_t queue_length(queue_t *queue)
{ return queue->head >= queue->tail ? queue->head - queue->tail : queue->head - queue->tail + queue->size; }

/* returns the bytes space available to write */
static inline uint8_t queue_space(queue_t *queue)
{ return queue->tail <= queue->head ? queue->tail - queue->head + queue->size - 1 : queue->tail - queue->head - 1; }

/* read a byte without dequeueing */
static inline uint8_t queue_peek(queue_t *queue, uint8_t address)
{ return queue->data[(queue->tail + address) & (queue->size - 1)]; }

/* write a byte without enqueueing */
static inline void queue_poke(queue_t *queue, uint8_t address, uint8_t byte)
{ queue->data[(queue->tail + address) & (queue->size - 1)] = byte; }

/* dequeue a single byte */
static inline uint8_t queue_getbyte(queue_t *queue)
{
  uint8_t byte;
  byte = queue->data[queue->tail++];
  queue->tail &= queue->size - 1;
  return byte;
}

/* enqueue a single byte */
static inline void queue_putbyte(queue_t *queue, uint8_t byte)
{
  queue->data[queue->head++] = byte;
  queue->head &= queue->size - 1;
}

#endif /* _QUEUE_H_ */

