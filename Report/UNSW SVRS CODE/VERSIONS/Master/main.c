#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include "softuart.h"
#include <string.h>
#include <util/crc16.h>
#include "main.h"
#include "lcd.h"
#include "queue.h"
//#include "stackmon.h"

//#define SIZE 10

// this global variable stores the current time in s
static volatile uint16_t time_irq;
extern uint8_t   inbuf[SOFTUART_IN_BUF_SIZE];
extern volatile unsigned char qin;
queue_t input_queue;

// Puts the CPU into sleep mode.
void __attribute__ ((__noinline__)) go_to_sleep(uint8_t mode) 
{
      set_sleep_mode(mode);
      cli();
      {
        sleep_enable();
        sei();
        sleep_cpu();
        sleep_disable();
      }
      sei();
} 

int main(void)
{
  uint8_t i;
  uint16_t good = 0;
  uint16_t bad = 0;
  uint16_t found = 0;
  queue_init(&input_queue, inbuf, SOFTUART_IN_BUF_SIZE); //make inbuf a queue
  char rx_string[TX_LENGTH] = {0};

  setupPinInterrupts();
  softuart_init();
  softuart_turn_rx_on(); /* redundant - on by default */
  // set up WDT
  WDTCR |= (1 << WDIE) | (1 << WDP2)| (1 << WDP1);

  // set up LCD
  LCDInit();
  LCDMove(0,0);
  LCDString("Good");
  LCDMove(1,0);
  LCDString("Bad");


  sei(); 
  for(;;) 
  {
    LCDMove(0,5);
    LCDHex16(good);
    LCDMove(1,5);
    LCDHex16(bad);
    LCDMove(0,11);
    LCDHex16(found);
    cli();
    if(queue_length(&input_queue) > TX_LENGTH) {

      //pull first two bytes out of queue. It should be address.
      //if both are successful, pull out 6 more bytes
      //do error check on 8 byte packet

      rx_string[0] = queue_getbyte(&input_queue);
      rx_string[1] = queue_peek(&input_queue, 0); 
      if(rx_string[0] == MY_ADDRESS && rx_string[1] == MY_NEXT_ADDR) { // if the address bytes are correct
        queue_getbyte(&input_queue); // remove that byte we peeked at without pulling
        for(i = 2; i < TX_LENGTH; i++) { // pull next 6 bytes from queue
          rx_string[i] = queue_getbyte(&input_queue);
        }
        sei();
        //error checking
        //compare expected result with calculated result.
        uint16_t check = 0;
        uint16_t expect = rx_string[CHECKSUM_BYTE-1] << 8 | rx_string [CHECKSUM_BYTE]; 
        for (int i = 0; i < TX_LENGTH - 2; i++) {
          check = _crc_xmodem_update(check, rx_string[i]);
        }
        if( check== expect) { 
          good++;
        } else {
          bad++;
          for (i = 0; i < TX_LENGTH ; i++) { // print rx_string to screen
            if(i < 5) {
              LCDMove(2, i * 3);
            } else {
              LCDMove(3, (i - 5) * 3);
            }
            LCDHex8(rx_string[i]);
          }
        }
      }
    }
    sei();
  }

  cli(); // global interrupt disable

  return 0; // never supposed to reach here.
}

ISR(WDT_vect)
{
  WDTCR |= (1 << WDIE); // must be done otherwise MCU will reset on the next WDT overflow
  time_irq++;           // this is despite the fact that we've already set this bit in setupWDT
}

void setupPWM(void) { 
  TCCR1 |=  (1<<CS12) ;
  GTCCR |= (1<<PWM1B) | (1 << COM1B0);
  OCR1B = PWM_OFF;
  OCR1C = PWM_MATCH_SIZE;
  TCNT1 = 0;
} 

void setupPinInterrupts (void) {
    GIMSK |= 1 << PCIE;
    PCMSK |= 1 << PCINT4;
}
