#ifndef _MAIN_H
#define _MAIN_H

#define HEATER_PORT PORTB
#define HEATER_DDR DDRB
#define HEATER (1<<PB3)

#define SWITCH_PORT PORTB
#define SWITCH_DDR DDRB
#define SWITCH (1<<PB4)

#define SERIAL_PORT PORTB
#define SERIAL_DDR DDRB
#define SERIAL_USCK (1 << PB2)
#define SERIAL_DO (1 << PB1)
#define SERIAL_DI (1 << PB0)

#define ADDRESS_BYTE  0
#define NEXT_ADDR_BYTE 1
#define VOLT_BYTE	  3
#define TEMP_BYTE	  5
#define CHECKSUM_BYTE 7
#define TX_LENGTH     8

#define ACCEPT		  1
#define REJECT		  0
#define YES			  1
#define NO			  0

#define MY_ADDRESS	 0x10
#define MY_NEXT_ADDR 0x20

#define BITS 8

#define PWM_MATCH_SIZE  26
#define PWM_OFF             26 // should be 0 unless using inverted PWM output, then use PWM_MATCH_SIZE.
#define PWM_ON              (1 * PWM_MATCH_SIZE / 2)

void setupADC(); // sets up ADC for measuring voltage on PB5
void setupTMP(); // sets up ADC for measuring temperature. Note it needs a delay between it and measuring tmp to
// allow for the ADC to settle after changing reference
uint16_t measureTMP(); // measures temperature
uint16_t measureVoltage(); // measures voltage
void setupTimer1(void); // sets up timer1 for something?
void toggleLED(void);
void delay(uint16_t ms); // delay in increments of 1/3600 of a second
void savePower(); // shuts down some peripherals
void powerOn(); // boots up the above peripherals
void setupWDT(); // sets up WDT for 1s ticks and instead of a reset an interrupt is called
void setupPWM(void); // sets up PWM for a frequency of 38kHz and duty cycle of 50%
void WDT_off(void); // turns WDT off
void pwmOn(); // turns PWM on
void pwmOff(); // turns PWM off 
void go_to_sleep(uint8_t mode); // sends microprocessor to sleep

void setupPinInterrupts (void); // sets up pin interrupts on PB4

//char *filter_rx( char rx_string[], char inbuf[]);
uint8_t checking(char rx_string[TX_LENGTH]); // performs xmodem 16-bit CRC on an 8 byte arrary or string

#endif /* _MAIN_H */

