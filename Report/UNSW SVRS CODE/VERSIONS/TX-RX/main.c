#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include "softuart.h"
#include <string.h>
#include <util/crc16.h>
#include "main.h"
#include "lcd.h"
//#include "stackmon.h"

//#define SIZE 10

// this global variable stores the current time in ms
static volatile uint16_t time_irq;
volatile uint16_t fast_time;
extern volatile char   inbuf[SOFTUART_IN_BUF_SIZE];

//static volatile uint8_t debug[SIZE] = {0};

// Puts the CPU into sleep mode.
void __attribute__ ((__noinline__)) go_to_sleep(uint8_t mode)
{
      set_sleep_mode(mode);
      cli();
      {
        sleep_enable();
        sei();
        sleep_cpu();
        sleep_disable();
      }
      sei();
} // Wakes up on any interrupt.

uint16_t myadc(void)
{

  ADCSRA |= _BV(ADIE);

  set_sleep_mode( SLEEP_MODE_ADC );
  sleep_enable();

  do
  {
    // The following line of code is only important on the second pass.  
    // For the first pass it has no effect.
    // Ensure interrupts are enabled before sleeping
    sei();
    // Sleep (MUST be called immediately after sei)
    sleep_cpu();
    // Checking the conversion status has to be done with interrupts 
    // disabled to avoid a race condition
    // Disable interrupts so the while below is performed without interruption
    cli();
  }
  // Conversion finished?  If not, loop.
  while( ( (ADCSRA & (1<<ADSC)) != 0 ) );

  sleep_disable();
  sei();

  ADCSRA &= ~ _BV( ADIE );
  return (ADC);
}

//static uint8_t heat_counter = 0;

int main(void)
{
  //HEATER_PORT &= ~HEATER; // force heater output low
  //HEATER_DDR |= HEATER; // set heater pin as output
  uint8_t i;
  uint8_t j;
  //uint8_t sent_to_me_flag = 0;
  uint16_t check = 0;
  char tx_string[TX_LENGTH] = {MY_ADDRESS, MY_NEXT_ADDR, 0x30, 0x40, 0x50, 0x60, 0x70, 0};
  char rx_string[TX_LENGTH] = {0};
  //char rx_byte = 0;
  uint16_t last_time = 0;

  //SWITCH_PORT &= ~SWITCH; // force switch output low
  //SWITCH_DDR |= SWITCH; // set switch pin as output
  setupPinInterrupts();
  softuart_init();
  softuart_turn_rx_on(); /* redundant - on by default */
  // set up WDT
  WDTCR |= (1 << WDIE) | (1 << WDP2)| (1 << WDP1);
  //WDT_off();
  setupPWM();
  setupADC();

  // set up LCD
  LCDInit();

  // set up PID
  //PIDInit();

  sei(); // global interrupt enable
  //  uint16_t temp=0;
  //go_to_sleep(SLEEP_MODE_IDLE);
  for(;;) 
  {

 
    cli(); // global interrupt disable
    uint16_t time = time_irq; // read time_irq atomically
    sei(); // global interrupt enable
    //uint16_t temp = measureTMP();
    //uint16_t volt = measureVoltage();
    //power_adc_enable();
    //power_usi_enable();
    //update LCD state
    if(time > last_time+4) {
      check = 0;
      /*measureVoltage(); // remove bunk result
      uint16_t volt = measureVoltage();
      setupTMP();

      tx_string[VOLT_BYTE - 1] = volt >> 8;
      tx_string[VOLT_BYTE] = volt & 0xFF;

      /*LCDMove(0, 0);
      LCDString("Time: ");
      LCDHex16(time); */
      /*delay(200);

      measureTMP(); // remove bunk result
      uint16_t temp = measureTMP();

      tx_string[TEMP_BYTE - 1] = temp >> 8;
      tx_string[TEMP_BYTE] = temp & 0xFF; */

      for (i = 0; i < TX_LENGTH - 2; i++) {
        check = _crc_xmodem_update(check, tx_string[i]);
      }
      tx_string[CHECKSUM_BYTE-1] = check >> 8;
      //check = check << 8;
      tx_string[CHECKSUM_BYTE] = check & 0xFF;

      //savePower();

      //power_timer0_enable(); // enable softuart timer
      softuart_puts(tx_string);
      /*for (i = 0; i < TX_LENGTH; i++) {
        if(i < 5) {
          LCDMove(0, i * 3);
        } else {
          LCDMove(1, (i - 5) * 3);
        }
        //LCDHex(tx_string[i],1);
      } */
      delay(200);

      cli();
      /*for (i = 0; i < SOFTUART_IN_BUF_SIZE; i++) {
        if(i < 5) {
          LCDMove(2, i * 3);
        } else {
          LCDMove(3, (i - 5) * 3);
        }
        LCDHex8(inbuf[i]);
      } */

      sei();

      cli(); // timing is really key here, you've got somehow make sure that this isn't getting in between
      // receiving the arrival of two characters
      for(i = 0; i < SOFTUART_IN_BUF_SIZE; i++) {
        if(inbuf[i] == MY_ADDRESS) {
          rx_string[ADDRESS_BYTE] = inbuf[i];
          for(j = 1; j < TX_LENGTH; j++) {
            if(j + i < SOFTUART_IN_BUF_SIZE) {
             rx_string[j] = inbuf[i+j];
            } else {
              rx_string[j] = inbuf[j + i - SOFTUART_IN_BUF_SIZE];
            }
          }
        }
      }
      /*if(rx_string[NEXT_ADDR_BYTE] != MY_NEXT_ADDR) {
        sent_to_me_flag = NO;
      } else {
        sent_to_me_flag = YES;
      } */
      sei();
      softuart_flush_input_buffer();
      //if (sent_to_me_flag) {  // if the address is correct
        for (i = 0; i < TX_LENGTH; i++) { // print rx_string to screen
          if(i < 5) {
            LCDMove(0, i * 3);
          } else {
            LCDMove(1, (i - 5) * 3);
          }
          LCDHex8(rx_string[i]);
        }
        if(checking(rx_string)) { // call error checking and print result to screen
          LCDMove(1,9);
          LCDString("Y");
        } else {
          LCDMove(1,9);
          LCDString("N");
        } 
      // }
      last_time = time;
      for(i = 2; i < TX_LENGTH - 2; i++) {
        tx_string[i]++;
      } 
      /*uint8_t spaceLeft = StackCount();
      LCDMove(3,0);
      LCDHex8(spaceLeft);*/
    }

    cli();
    for (i = 0; i < SOFTUART_IN_BUF_SIZE; i++) {
      if(i < 5) {
        LCDMove(2, i * 3);
      } else {
        LCDMove(3, (i - 5) * 3);
      }
      LCDHex8(inbuf[i]);
    } 
    sei();

    go_to_sleep(SLEEP_MODE_IDLE);
  }

  cli(); // global interrupt disable

  return 0; // never supposed to reach here.
}

// TIMER0 compare interrupt service routine
/*ISR(TIM0_COMPA_vect)
{
  // count milliseconds in time_irq variable
  time_irq++;
} */

uint8_t checking(char rx_string[TX_LENGTH]) {
  uint16_t check = 0;
  uint16_t expect = rx_string[CHECKSUM_BYTE-1] << 8 | rx_string [CHECKSUM_BYTE];
  //LCDMove(1, 11);
  //LCDHex16(expect); 
  for (int i = 0; i < TX_LENGTH - 2; i++) {
    check = _crc_xmodem_update(check, rx_string[i]);
  }

  //LCDMove(2, 11);
  //LCDHex16(check); 

  if (check == expect) {
    return ACCEPT;
  } else {
    return REJECT;
  }

}

// ADC conversion interrupt service routine
ISR(ADC_vect)
{
  // store the latest temp reading in temp_irq
 // temp_irq = ADCL | (ADCH << 8);
}

ISR(WDT_vect) // DUNNO WHAT THIS DOES, BETTER ASK.
{
  /*uint8_t t = TCNT1;
  if (time_wdt != 0 && !captured) {
    delta_wdt = t - time_wdt;
    adc_captured = temp;
    captured = 1;
  }
  time_wdt = t; */
  WDTCR |= (1 << WDIE);
  time_irq++;
}

/*void setupWDT() { // aim for 1 s
  WDTCR |= (1 << WDIE) | (1 << WDP2)| (1 << WDP1);
  // what about those safety levels though?
}*/

/*void savePower() { // turns off timer0 (used for uart), USI and ADC
  power_timer0_disable();
  //power_adc_disable();
  power_usi_disable();
}

void powerOn() { // turns on timer0, USI and ADC
  power_timer0_enable();
  //power_adc_enable();
  power_usi_enable();
} */

void delay(uint16_t ms) { // delay function enter the number of time periods
  uint16_t start_time = fast_time;
  while (fast_time < start_time + ms){
    go_to_sleep(SLEEP_MODE_IDLE);
  } // largely useless if you need to do accurate timing. For longer term things though it's okay.
} 

void setupPWM(void) { // for the moment PWM and uart use the same pin. Until I get
  // a dev kit or breadboard version running you can only do one
  TCCR1 |=  (1<<CS12) ;
  GTCCR |= (1<<PWM1B) | (1 << COM1B0);
  OCR1B = PWM_OFF;
  OCR1C = PWM_MATCH_SIZE;
  TCNT1 = 0;
} // need to determine frequency...

void setupADC() {
  // set up ADC for reading voltage
  // ADMUX |= (1<<REFS1)| (1<<REFS2); // select internal reference
  ADMUX = (1<<REFS2)|(1<<REFS1);
  ADMUX |= (1<<MUX1) | (1<<MUX0); // select voltage
  ADCSRA |= (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0); // slow down ADC, 128X prescaler
  // ADCSRA |= (1<<ADEN) | (1<<ADATE) | (1<<ADIE) | (1<<ADSC); // enable ADC, free-running
  ADCSRA |= (1<<ADEN); 
}

void setupTMP() {
  ADMUX = (1<<REFS1);
}

uint16_t measureTMP() { // basically a wrapper for myadc to ensure it's measuring tmp
  //ADMUX = (1<<REFS1);
  ADMUX |= (1<<MUX3) | (1<<MUX2) | (1<<MUX1) | (1<<MUX0);
  return myadc();
}

uint16_t measureVoltage() { // basically a wrapper for myadc to measure voltage instead of temp

  ADMUX = (1<<REFS2)|(1<<REFS1);
 //ADMUX |= (1<<MUX1) | (1<<MUX0); // select voltage  
  return myadc();
}

/*void toggleLED(void) {
	SWITCH_PORT ^= SWITCH;
} */

/*ISR(TIM1_COMPA_vect) // TIMER1 INTERRUPT HANDLER
{
  TCNT1 = 0; // gotta reset timer for some reason, I don't know why.
  //SWITCH_PORT ^= SWITCH;
} */

/*void pwmOn() { // with post setup in mind
  OCR1B = PWM_ON;
}

void pwmOff() {
  OCR1B = PWM_OFF;
} */

void setupPinInterrupts (void) {
    GIMSK |= 1 << PCIE;
    PCMSK |= 1 << PCINT4;
}
