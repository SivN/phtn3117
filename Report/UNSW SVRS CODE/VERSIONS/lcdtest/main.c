#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "main.h"
#include "lcd.h"

int main(void)
{
  LCDInit();

  for(;;)
  {
    LCDMove(0, 0);
    LCDString("Hello, world!");
  }
}

