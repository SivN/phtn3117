#ifndef _LCD_H_
#define _LCD_H_

void LCDInit();
void LCDMove(uint8_t row, uint8_t col);
void LCDString(char *str);
void LCDHex8(uint8_t val);
//void LCDHex16(uint16_t val);
void LCDHex32(uint32_t val);

#endif /* _LCD_H_ */

