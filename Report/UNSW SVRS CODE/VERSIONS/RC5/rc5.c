#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include "main.h"
#include "rc5.h"
#include "lcd.h"

#define TX_MASK ((1<<0) | (1 << 1))
#define MATCH_VAL 10  // work this out later may not be quite right

static volatile uint16_t tx_buffer; // but only use 14 bits of this

static volatile uint16_t mask;
static volatile uint8_t counter = 0;
static volatile uint8_t pwmFlag;

ISR(TIM0_COMPA_vect) {
	static volatile uint8_t pwmFlag;
	static volatile uint8_t bit_flag = 0; // signifies if we're halfway through bit
	// read tx_buffer 
	// if bit_flag == 0
	// if it's a 1, turn pwm off
	// if it's a 0 turn pwm on
	// then set bit_flag to 1
	// if bit_flag == 1
	// toggle PWM and shift through tx_buffer by 1
	if (counter <= 13) {
		if (bit_flag) { // send half of bit
			/*if (pwmFlag == 1){ 
				pwmOn();
				pwmFlag = 0;
			} else {
				pwmOn();
				pwmFlag = 1;
			} */
			pwmOn();
			//SWITCH_PORT |= SWITCH;
			//mask = mask << 1;
			bit_flag = 0;
			counter++;
		} else { // first half of bit
			bit_flag = 1;
			/*if ((tx_buffer & mask)==1) {
				pwmOn();
				pwmFlag = 0;
			} else {
				pwmOn();
				pwmFlag = 1;
			} */
			pwmOff();
			//SWITCH_PORT &= ~SWITCH;
		}
	} else {
		pwmOff();
	} 
    /*LCDMove(1,0);
    LCDString("Counter:");
    LCDHex8(counter); */
}



void pwmOn() { // with post setup in mind
	OCR1B = PWM_ON;
}

void pwmOff() {
	OCR1B = PWM_OFF;
}

void setupRC5() {
  TCNT0 = 0; // set initial timer value
  OCR0A = MATCH_VAL; // set match register value
  TCCR0A |= (1<<WGM01); // set clear timer on compare match mode
  TCCR0B |= (1<<CS01) | (1<<CS00); // start timer, 64X prescaler
  TIMSK |= (1<<OCIE0A); // enable compare match interrupt
}

// SEND LSB FIRST
void send_data(uint16_t to_send) { // TWO LSB are reserved
	tx_buffer = to_send << 2;;
	tx_buffer |= TX_MASK;
	counter = 0;
	mask = 1;
}
