#include <avr/io.h>
#include <util/delay.h>

#include "lcd.h"

#define LCD_ROWS 4
#define LCD_COLS 16

#define LCD_PORT PORTB
#define LCD_DDR DDRB
#define LCD_DA (1<<PB0) // MOSI
#define LCD_EN (1<<PB1) // MISO
#define LCD_CL (1<<PB2) // SCK

void __attribute__ ((__noinline__)) SendByte(uint8_t byte, uint8_t rs, uint8_t delay)
{

  for (uint8_t i = 0; i < 8; i++) {
    if(byte & 1<<(7 - i))
      LCD_PORT |= LCD_DA; // data high
    else
      LCD_PORT &= ~LCD_DA; // data low

    LCD_PORT |= LCD_CL; // clock high
    _delay_us(1);
    LCD_PORT &= ~LCD_CL; // clock low
  }

  // 74HC595 needs one more clock...
  LCD_PORT |= LCD_CL; // clock high
  _delay_us(1);
  LCD_PORT &= ~LCD_CL; // clock low

  if(rs)
    LCD_PORT |= LCD_DA; // data high
  else
    LCD_PORT &= ~LCD_DA; // data low

  LCD_PORT |= LCD_EN; // enable high
  _delay_us(1);
  LCD_PORT &= ~LCD_EN; // enable low
  for (uint8_t i = 0; i < delay; i++) {
    _delay_us(40);
  }
}

static uint8_t initp[] = {0x30, 0x30, 0x30, 0x38, 0x08, 0x01, 0x02, 0x06, 0x0f, 0};
static uint8_t initq[] = { 125,    3,    1,    1,    1,   50,   50,    1,    1};

void LCDInit()
{
    uint8_t *p = initp;
    uint8_t *q = initq;

    LCD_DDR |= LCD_DA | LCD_EN | LCD_CL;

    _delay_ms(40);

    while (*p) {
      SendByte(*p++, 0, *q++);
    }
}

void LCDMove(uint8_t row, uint8_t col)
{
    uint8_t ddram_addr = 0x80;

    ddram_addr += col;
 
    if (row & 1)
      ddram_addr += 0x40;

    if (row > 1)
      ddram_addr += LCD_COLS;

    SendByte(ddram_addr, 0, 1);
}

void LCDString(char *str)
{
  while (*str) {
    SendByte(*str++, 1, 1);
  }
}

static const uint8_t hex_digits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

void LCDHex8(uint8_t val)
{
   for (uint8_t i = 4; i < 5; i -= 4) {
     SendByte(hex_digits[(val >> i) & 0x0f], 1, 1);
   }
}

void LCDHex16(uint16_t val)
{
   for (uint8_t i = 12; i < 13; i -= 4) {
     SendByte(hex_digits[(val >> i) & 0x0f], 1, 1);
   }
}

/*
void LCDHex32(uint32_t val)
{
   for (uint8_t i = 28; i < 29; i -= 4) {
     SendByte(hex_digits[(val >> i) & 0x0f], 1, 1);
   }
}
*/
