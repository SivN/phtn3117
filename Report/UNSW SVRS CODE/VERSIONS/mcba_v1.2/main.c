#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include "softuart.h"
#include "main.h"
#include "lcd.h"
#include "pid.h"

// this global variable stores the current time in ms
static volatile uint16_t time_irq;

// this global variable stores the latest temp reading
static volatile uint16_t temp_irq;
static volatile uint8_t time_wdt = 0, delta_wdt;
static volatile uint8_t captured = 0;
static volatile uint16_t adc_captured = 0, temp = 0;

// Puts the CPU into sleep mode.
void go_to_sleep(uint8_t mode)
{
      set_sleep_mode(mode);
      cli();
      {
        sleep_enable();
        sei();
        sleep_cpu();
        sleep_disable();
      }
      sei();
}

uint16_t myadc(void)
{

  ADCSRA |= _BV(ADIE);

  set_sleep_mode( SLEEP_MODE_ADC );
  sleep_enable();

  do
  {
    // The following line of code is only important on the second pass.  
    // For the first pass it has no effect.
    // Ensure interrupts are enabled before sleeping
    sei();
    // Sleep (MUST be called immediately after sei)
    sleep_cpu();
    // Checking the conversion status has to be done with interrupts 
    // disabled to avoid a race condition
    // Disable interrupts so the while below is performed without interruption
    cli();
  }
  // Conversion finished?  If not, loop.
  while( ( (ADCSRA & (1<<ADSC)) != 0 ) );

  sleep_disable();
  sei();

  ADCSRA &= ~ _BV( ADIE );
  return (ADC);
}

//static uint8_t heat_counter = 0;

int main(void)
{
  //HEATER_PORT &= ~HEATER; // force heater output low
  //HEATER_DDR |= HEATER; // set heater pin as output

  SWITCH_PORT &= ~SWITCH; // force switch output low
  SWITCH_DDR |= SWITCH; // set switch pin as output

  softuart_init();
  softuart_turn_rx_on(); /* redundant - on by default */

  setupTimer1();
  set_sleep_mode(SLEEP_MODE_IDLE); // so we can still do normal interrupts.

//  TCNT1 = 0;
//  TCCR1 = 0x0F;
  //_WDR();
  //MCUSR = 0x00;
 // WDTCR |= (1<<WDCE) | (1<<WDE);
 // WDTCR = 0x00;
//PLLCSR |= (1<<PLLE);
 //PLLCSR |= (1<<PCKE);
 //WDTCR |= (1<<WDIE)|(1<<WDP3)|0x01;

  // set up ADC for reading temp
 // ADMUX |= (1<<REFS1)| (1<<REFS2); // select internal reference
 ADMUX |= (1<<REFS1);
  ADMUX |= (1<<MUX3) | (1<<MUX2) | (1<<MUX1) | (1<<MUX0); // select temp sensor input
  ADCSRA |= (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0); // slow down ADC, 128X prescaler
 // ADCSRA |= (1<<ADEN) | (1<<ADATE) | (1<<ADIE) | (1<<ADSC); // enable ADC, free-running
  ADCSRA |= (1<<ADEN); 
  // set up LCD
  LCDInit();

  // set up PID
  //PIDInit();

  sei(); // global interrupt enable
  //  uint16_t temp=0;
  for(;;) // loop forever
  {

    // Save power by disabling the ADC, going to sleep, and then re-enabling
    // the ADC. Sleep mode is exited when the watchdog timer interrupt occurs.
    //   ADCSRA &= ~ADEN;
    //    go_to_sleep(SLEEP_MODE_IDLE);
    //go_to_sleep(SLEEP_MODE_PWR_DOWN);
    //ADCSRA |= (1<<ADEN);
 
    cli(); // global interrupt disable
    uint16_t time = time_irq; // read time_irq atomically
    uint16_t temp ; // read temp_irq atomically
    uint16_t myt; 
  //time = TCNT0;
    sei(); // global interrupt enable
    myt = TCNT1;
   // static uint8_t heat; // heater proportional control value

    static uint16_t last_1hz_time;
    if(time - last_1hz_time > 0)
    {
      last_1hz_time = time;
      // code here executes at 1Hz
      // e.g. update LCD state
      LCDMove(0, 0);
      LCDString("Time: ");
      myt = time;
      LCDHex16(myt);

      LCDMove(1, 0);
      LCDString("Temp: ");
      temp = myadc();
      LCDHex16(temp); 


    }
// flash LED
    static uint16_t last_10hz_time;
    if(time - last_10hz_time > 100)
    {
      last_10hz_time = time;

      // code here executes at 10Hz
      //toggleLED();
      softuart_putchar(0b01011010);

    }

    static uint16_t last_100hz_time;
    if(time - last_100hz_time > 10)
    {
      last_100hz_time = time;
    }

  }

  cli(); // global interrupt disable

  return 0; // never supposed to reach here.
}

// TIMER0 compare interrupt service routine
/*ISR(TIM0_COMPA_vect)
{
  // count milliseconds in time_irq variable
  time_irq++;
} */

// ADC conversion interrupt service routine
ISR(ADC_vect)
{
  // store the latest temp reading in temp_irq
 // temp_irq = ADCL | (ADCH << 8);
}

ISR(WDT_vect)
{
  uint8_t t = TCNT1;
  if (time_wdt != 0 && !captured) {
    delta_wdt = t - time_wdt;
    adc_captured = temp;
    captured = 1;
  }
  time_wdt = t;
}



void delay(int ms) { // delay function enter the number of milliseconds here
  uint16_t start_time = time_irq;
  while (time_irq < start_time + ms){
  } // largely useless if you need to do accurate timing. For longer term things though it's okay.
}

void setupTimer1(void) { 
  // set up timer to be okay


  // set up TIMER1 for 1ms ticks
  TCNT1 = 0; // set initial timer value
  OCR1A = 125; 
  //TCCR1 = (1 << CTC1);
  TCCR1 |=  (1 << CS10)   | (1 << CS11) | (1 << CS12) /*|(1 << CS13) */ ;
	TIMSK |= (1 << OCIE1A);
  
} 

void toggleLED(void) {
	SWITCH_PORT ^= SWITCH;
}

ISR(TIM1_COMPA_vect) // TIMER1 INTERRUPT HANDLER
{
  TCNT1 = 0; // gotta reset timer for some reason, I don't know why.
  //SWITCH_PORT ^= SWITCH;
  time_irq++;
}