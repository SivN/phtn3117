#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include "softuart.h"
#include "main.h"
#include "lcd.h"
#include "pid.h"

// this global variable stores the current time in ms
static volatile uint16_t time_irq;

// this global variable stores the latest temp reading
static volatile uint16_t temp_irq;
static volatile uint8_t time_wdt = 0, delta_wdt;
static volatile uint8_t captured = 0;
static volatile uint16_t adc_captured = 0, temp = 0;

// Puts the CPU into sleep mode.
void go_to_sleep(uint8_t mode)
{
      set_sleep_mode(mode);
      cli();
      {
        sleep_enable();
        sei();
        sleep_cpu();
        sleep_disable();
      }
      sei();
} // Wakes up on any interrupt.

uint16_t myadc(void)
{

  ADCSRA |= _BV(ADIE);

  set_sleep_mode( SLEEP_MODE_ADC );
  sleep_enable();

  do
  {
    // The following line of code is only important on the second pass.  
    // For the first pass it has no effect.
    // Ensure interrupts are enabled before sleeping
    sei();
    // Sleep (MUST be called immediately after sei)
    sleep_cpu();
    // Checking the conversion status has to be done with interrupts 
    // disabled to avoid a race condition
    // Disable interrupts so the while below is performed without interruption
    cli();
  }
  // Conversion finished?  If not, loop.
  while( ( (ADCSRA & (1<<ADSC)) != 0 ) );

  sleep_disable();
  sei();

  ADCSRA &= ~ _BV( ADIE );
  return (ADC);
}

//static uint8_t heat_counter = 0;

int main(void)
{
  //HEATER_PORT &= ~HEATER; // force heater output low
  //HEATER_DDR |= HEATER; // set heater pin as output

  SWITCH_PORT &= ~SWITCH; // force switch output low
  SWITCH_DDR |= SWITCH; // set switch pin as output

  softuart_init();
  softuart_turn_rx_on(); /* redundant - on by default */
  //setupTimer1();
  setupWDT();
  power_timer1_disable();
//  TCNT1 = 0;
//  TCCR1 = 0x0F;
  //_WDR();
  //MCUSR = 0x00;
 // WDTCR |= (1<<WDCE) | (1<<WDE);
 // WDTCR = 0x00;
//PLLCSR |= (1<<PLLE);
 //PLLCSR |= (1<<PCKE);
 //WDTCR |= (1<<WDIE)|(1<<WDP3)|0x01;

  // set up ADC for reading temp
 // ADMUX |= (1<<REFS1)| (1<<REFS2); // select internal reference
 ADMUX |= (1<<REFS1);
  ADMUX |= (1<<MUX3) | (1<<MUX2) | (1<<MUX1) | (1<<MUX0); // select temp sensor input
  ADCSRA |= (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0); // slow down ADC, 128X prescaler
 // ADCSRA |= (1<<ADEN) | (1<<ADATE) | (1<<ADIE) | (1<<ADSC); // enable ADC, free-running
  ADCSRA |= (1<<ADEN); 
  // set up LCD
  LCDInit();

  // set up PID
  //PIDInit();

  sei(); // global interrupt enable
  //  uint16_t temp=0;
  go_to_sleep(SLEEP_MODE_IDLE);
  for(;;) // loop forever at 10Hz
  {
    savePower();

    // Save power by disabling the ADC, going to sleep, and then re-enabling
    // the ADC. Sleep mode is exited when the watchdog timer interrupt occurs.
    //   ADCSRA &= ~ADEN;
    //    go_to_sleep(SLEEP_MODE_IDLE);
    //go_to_sleep(SLEEP_MODE_PWR_DOWN);
    //ADCSRA |= (1<<ADEN);
 
    cli(); // global interrupt disable
    uint16_t time = time_irq; // read time_irq atomically
    uint16_t temp ; // read temp_irq atomically
    uint16_t myt; 
  //time = TCNT0;
    sei(); // global interrupt enable
    power_adc_enable();
    power_usi_enable();
    //update LCD state
    LCDMove(0, 0);
    LCDString("Time: ");
    myt = time;
    LCDHex16(myt);

    LCDMove(1, 0);
    LCDString("Temp: ");
    temp = myadc();
    LCDHex16(temp); 
    savePower();

    power_timer0_enable(); // enable softuart timer
    softuart_putchar('a');
    savePower();

    go_to_sleep(SLEEP_MODE_IDLE);
  }

  cli(); // global interrupt disable

  return 0; // never supposed to reach here.
}

// TIMER0 compare interrupt service routine
/*ISR(TIM0_COMPA_vect)
{
  // count milliseconds in time_irq variable
  time_irq++;
} */

// ADC conversion interrupt service routine
ISR(ADC_vect)
{
  // store the latest temp reading in temp_irq
 // temp_irq = ADCL | (ADCH << 8);
}

ISR(WDT_vect) // DUNNO WHAT THIS DOES, BETTER ASK.
{
  /*uint8_t t = TCNT1;
  if (time_wdt != 0 && !captured) {
    delta_wdt = t - time_wdt;
    adc_captured = temp;
    captured = 1;
  }
  time_wdt = t; */
  WDTCR |= (1 << WDIE);
  time_irq++;
}

void setupWDT() { // aim for 1 s
  WDTCR |= (1 << WDIE) | (1 << WDP2)| (1 << WDP1);
  // what about those safety levels though?
}

void savePower() { // turns off timer0 (used for uart), USI and ADC
  power_timer0_disable();
  power_adc_disable();
  power_usi_disable();
}

void powerOn() { // turns on timer0, USI and ADC
  power_timer0_enable();
  power_adc_enable();
  power_usi_enable();
}

void delay(int ms) { // delay function enter the number of milliseconds here
  uint16_t start_time = time_irq;
  while (time_irq < start_time + ms){
  } // largely useless if you need to do accurate timing. For longer term things though it's okay.
}

void setupTimer1(void) { // for legit power saving mode space timer things out
  // set up timer to be okay


  // set up TIMER1 for 0.1s ticks
  TCNT1 = 0; // set initial timer value
  OCR1A = 48; 
  //TCCR1 = (1 << CTC1);
  TCCR1 |=  (1 << CS10)   | (1 << CS11) | (1 << CS12) |(1 << CS13) ;
	TIMSK |= (1 << OCIE1A);
  
} 

void toggleLED(void) {
	SWITCH_PORT ^= SWITCH;
}

ISR(TIM1_COMPA_vect) // TIMER1 INTERRUPT HANDLER
{
  TCNT1 = 0; // gotta reset timer for some reason, I don't know why.
  //SWITCH_PORT ^= SWITCH;
  //time_irq++;
}