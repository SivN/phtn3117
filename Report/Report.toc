\contentsline {section}{List of Figures}{2}{section*.2}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}Explanation of Product and Design Concept}{4}{section.2}
\contentsline {section}{\numberline {3}Market analysis}{6}{section.3}
\contentsline {section}{\numberline {4}Manufacturing Cost Estimates}{9}{section.4}
\contentsline {section}{\numberline {5}Business Plan}{11}{section.5}
\contentsline {section}{\numberline {6}Development Plan}{12}{section.6}
\contentsline {section}{\numberline {7}Summary}{15}{section.7}
\contentsline {section}{References}{17}{section*.12}
\contentsline {section}{Appendix A: Requirements}{17}{section*.12}
\contentsline {section}{Appendix B: Survey}{17}{Item.19}
\contentsline {section}{Appendix C: Bill of Materials}{26}{section*.14}
