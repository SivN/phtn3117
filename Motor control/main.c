
#include "msp430f5529.h" // make sure you change the header to suit your particular device.
//#include "msp430f5xx_6xxgeneric.h"

// Connect the servo SIGNAL wire to P1.2 through a 1K resistor.

#define MCU_CLOCK           1000000
#define PWM_FREQUENCY       50      // In Hertz, ideally 50Hz.

#define SERVO_STEPS         180     // Maximum amount of steps in degrees (180 is common)
#define SERVO_MIN           700     // The minimum duty cycle for this servo
#define SERVO_MAX           2500   // The maximum duty cycle

//#define XT1_XT2_PORT_SEL            P5SEL
//#define XT1_ENABLE                  (BIT4 + BIT5)
//#define XT2_ENABLE                  (BIT2 + BIT3)

unsigned int PWM_Period     = (MCU_CLOCK / PWM_FREQUENCY);  // PWM Period
unsigned int PWM_Duty       = 0;


void main (void){

    unsigned int servo_stepval, servo_stepnow;
    unsigned int servo_lut[ SERVO_STEPS+1 ];
    unsigned int i;


    // Calculate the step value and define the current step, defaults to minimum.
    servo_stepval   = ( (SERVO_MAX - SERVO_MIN) / SERVO_STEPS );
    servo_stepnow   = SERVO_MIN;

    // Fill up the LUT
    for (i = 0; i < SERVO_STEPS; i++) {
        servo_stepnow += servo_stepval;
        servo_lut[i] = servo_stepnow;
    }

    // Setup the PWM, etc.
    WDTCTL  = WDTPW + WDTHOLD;     // Kill watchdog timer
    TA0CCTL1 = OUTMOD_7;            // Setting capture/compare control 1 to reset/set mode
    TA0CTL   = TASSEL_2 + MC_1;     // Timer control register: source -> SMCLK, mode -> Up mode; counts from zero to value of TA0CCR0.
    //TA0CTL = (TA0CTL|ID0|ID1);
    TA0CCR0  = PWM_Period-1;        // PWM Period, TA0CTL will count up to this and reset.
    TA0CCR1  = PWM_Duty;            // TACCR1 PWM Duty Cycle
    P1DIR   |= BIT2;               // P1.2 = output
    P1SEL   |= BIT2;               // P1.2 = TA1 output

    // Main loop
    while (1){

        // Go to 0�
        TA0CCR1 = servo_lut[0];
        __delay_cycles(1000000);

        // Go to 90�
        TA0CCR1 = servo_lut[90];
        __delay_cycles(1000000);

   }
}
