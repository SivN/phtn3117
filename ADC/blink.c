//***************************************************************************************
//  MSP430 Blink the LED Demo - Software Toggle P1.0
//
//  Description; Toggle P1.0 by xor'ing P1.0 inside of a software loop.
//  ACLK = n/a, MCLK = SMCLK = default DCO
//
//                MSP430x5xx
//             -----------------
//         /|\|              XIN|-
//          | |                 |
//          --|RST          XOUT|-
//            |                 |
//            |             P1.0|-->LED
//
//  J. Stevenson
//  Texas Instruments, Inc
//  July 2011
//  Built with Code Composer Studio v5
//***************************************************************************************

#include <msp430.h>				

int adc_val;

void init_adc();

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer
	P1DIR |= 0x01;					// Set P1.0 to output direction

	__bis_SR_register(GIE);

	for(;;) {
		volatile unsigned int i;	// volatile to prevent optimization

		P1OUT ^= 0x01;				// Toggle P1.0 using exclusive-OR

		i = 10000;					// SW Delay
		do i--;
		while(i != 0);
	}
	
	return 0;
}

void init_adc() {

	P6SEL |= BIT6;
	ADC12CTL0 = ADC12SHT00 | ADC12ON | ADC12REFON | ADC12REF2_5V;
	// adc is on with 2.5V reference turned on, and with sample and hold time of 4 cycles
	ADC12CTL2 = ADC12RES1 | ADC12RES0; // turn on max res
	ADC12MCTL0 = ADC12INCH_6;
}

#pragma vector=ADC12_VECTOR
__interrupt void ADC12ISR() {
	adc_val = ADC12MEM0;
}
