/*
 * main.c - get weather details sample application
 *
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Application Name     -   Get weather
 * Application Overview -   This is a sample application demonstrating how
 *                          to connect to openweathermap.org server and request
 *                          for weather details of a city. The application\
 *                          opens a TCP socket w/ the server and sends a HTTP
 *                          Get request to get the weather details. The received
 *                          data is processed and displayed on the console
 * Application Details  -   http://processors.wiki.ti.com/index.php/CC31xx_Get_Weather_Application
 *                          doc\examples\get_weather.pdf
 */

#include "simplelink.h"
#include "sl_common.h"
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include "driverlib.h"
#include <math.h>
//#include "msp430f5529.h" // For PWM control

#define APPLICATION_VERSION "1.2.0"

#define SL_STOP_TIMEOUT        0xFF

#define CITY_NAME       "Sydney"

/*#define WEATHER_SERVER  "openweathermap.org"

#define PREFIX_BUFFER   "GET /data/2.5/weather?q="
#define POST_BUFFER     "&mode=xml&units=imperial HTTP/1.1\r\nHost:api.openweathermap.org\r\nAccept:"
#define POST_BUFFER2    "*\r\n\r\n"*/

#define WEATHER_SERVER  "empset.com"

#define PREFIX_BUFFER   "GET http://empset.com HTTP/1.1"
#define POST_BUFFER     "\r\nHost:empset.com\r\nAccept:"
#define POST_BUFFER2    "*/*\r\n\r\n"

#define PUT_FIRST_BUF	"GET http://empset.com/cgi-bin/put.cgi?user=jim&pass=bitcoin"
#define PUT_BUF_TIME	"&time="
#define PUT_BUF_SPACE	"%3A"
#define PUT_BUF_FOOD	"&food="
#define PUT_BUF_LEFT	"&left="
#define PUT_SEC_BUF		" HTTP/1.1 \r\nHost:empset.com\r\nAccept: */*\r\n\r\n"

#define SMALL_BUF           32
#define MAX_SEND_BUF_SIZE   512
#define MAX_SEND_RCV_SIZE   1024

#define MAX_MASS_STRING_LENGTH 6
#define MAX_TIME_STRING_LENGTH 6
#define MASS_OFFSET 8
#define TIME_OFFSET 6
#define SYS_TIME_OFFSET 23
#define MAX_FEED_TIMES 16
#define GMT_TO_AEST 11 // since daylight saving has begun
#define HOURS_IN_DAY 24

#define MCU_CLOCK 3125000 // 25MHz/8
#define PWM_FREQUENCY 62500// Above value/50
#define SERVO_STEPS 180  // Maximum amount of steps in degrees (180 is common)
#define SERVO_MIN 347  // The minimum duty cycle for this servo
#define SERVO_MAX 62500// The maximum duty cycle
#define OPEN_SERVO SERVO_MAX - 6250
#define CLOSE_SERVO 6250/2

#define YES 1
#define NO 0
#define THRESHOLD 100

#define CHECK	0
#define TIME	1
#define PUT		2

unsigned int PWM_Period = (MCU_CLOCK / PWM_FREQUENCY);  // PWM Period
unsigned int PWM_Duty = 0;

int curHour;
int curMin;

// ADC Constants

#define RM 		10000
#define PRELIM 409600000

/* Application specific status/error codes */
typedef enum{
    DEVICE_NOT_IN_STATION_MODE = -0x7D0,        /* Choosing this number to avoid overlap with host-driver's error codes */
    HTTP_SEND_ERROR = DEVICE_NOT_IN_STATION_MODE - 1,
    HTTP_RECV_ERROR = HTTP_SEND_ERROR - 1,
    HTTP_INVALID_RESPONSE = HTTP_RECV_ERROR -1,

    STATUS_CODE_MAX = -0xBB8
}e_AppStatusCodes;

/*
 * GLOBAL VARIABLES -- Start
 */
_u32  g_Status = 0;
struct{
    _u8 Recvbuff[MAX_SEND_RCV_SIZE];
    _u8 SendBuff[MAX_SEND_BUF_SIZE];

    _u8 HostName[SMALL_BUF];
    _u8 CityName[SMALL_BUF];

    _u32 DestinationIP;
    _i16 SockID;
}g_AppData;
/*
 * GLOBAL VARIABLES -- End
 */


/*
 * STATIC FUNCTION DEFINITIONS -- Start
 */
static _i32 establishConnectionWithAP();
static _i32 disconnectFromAP();
static _i32 configureSimpleLinkToDefaultState();

static _i32 initializeAppVariables();
static void  displayBanner();

static _i32 getWeather(int *amount);
static _i32 getHostIP();
static _i32 createConnection();
static _i32 getData(int *amount);

uint32_t calculate_Rf (int result);
double calculate_force(uint32_t Rf);
void init_ADC(void);
void putWrapper(int timeHour, int timeMin, double amount, double left);
void timeWrapper(int *presentHour, int *presentMin);

void putData(int timeHour, int timeMin, double amount, double left);
int getTime(int *presentHour, int *presentMin);


double get_Force(void);

/*
 * STATIC FUNCTION DEFINITIONS -- End
 */

void init_ADC(void) {
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P6, GPIO_PIN0);

    ADC12_A_init(ADC12_A_BASE,
                 ADC12_A_SAMPLEHOLDSOURCE_SC,
                 ADC12_A_CLOCKSOURCE_ADC12OSC,
                 ADC12_A_CLOCKDIVIDER_1);

    ADC12_A_enable(ADC12_A_BASE);

    ADC12_A_setupSamplingTimer(ADC12_A_BASE,
                               ADC12_A_CYCLEHOLD_64_CYCLES,
                               ADC12_A_CYCLEHOLD_4_CYCLES,
                               ADC12_A_MULTIPLESAMPLESDISABLE);

    ADC12_A_configureMemoryParam param = {0};
    param.memoryBufferControlIndex = ADC12_A_MEMORY_0;
    param.inputSourceSelect = ADC12_A_INPUT_A0;
    param.positiveRefVoltageSourceSelect = ADC12_A_VREFPOS_AVCC;
    param.negativeRefVoltageSourceSelect = ADC12_A_VREFNEG_AVSS;
    param.endOfSequence = ADC12_A_NOTENDOFSEQUENCE;
    ADC12_A_configureMemory(ADC12_A_BASE,&param);
}

double get_Force(void) {
    ADC12_A_startConversion(ADC12_A_BASE,
                            ADC12_A_MEMORY_0,
                            ADC12_A_SINGLECHANNEL);

    //Poll for interrupt on memory buffer 0
    while(!ADC12_A_getInterruptStatus(ADC12_A_BASE,
                                      ADC12IFG0))
    {
        ;
    }

    //SET BREAKPOINT HERE
    volatile int result = ADC12MEM0;
    volatile uint32_t Rf = calculate_Rf(result);
    volatile double force = calculate_force(Rf);

     return force;
}

uint32_t calculate_Rf (int result) {
	uint32_t r_f = (PRELIM/result) - RM;
	return r_f;
}

double calculate_force(uint32_t Rf) {
	long long magicNo = 3280197600;
	double power = -1.3792 * log((double)Rf); // careful, this may provoke errors
	return magicNo * exp(power);
}

/*
 * ASYNCHRONOUS EVENT HANDLERS -- Start
 */

/*!
    \brief This function handles WLAN events

    \param[in]      pWlanEvent is the event passed to the handler

    \return         None

    \note

    \warning
*/

void SimpleLinkWlanEventHandler(SlWlanEvent_t *pWlanEvent)
{
    if(pWlanEvent == NULL)
        CLI_Write(" [WLAN EVENT] NULL Pointer Error \n\r");
    
    switch(pWlanEvent->Event)
    {
        case SL_WLAN_CONNECT_EVENT:
        {
            SET_STATUS_BIT(g_Status, STATUS_BIT_CONNECTION);

            /*
             * Information about the connected AP (like name, MAC etc) will be
             * available in 'slWlanConnectAsyncResponse_t' - Applications
             * can use it if required
             *
             * slWlanConnectAsyncResponse_t *pEventData = NULL;
             * pEventData = &pWlanEvent->EventData.STAandP2PModeWlanConnected;
             *
             */
        }
        break;

        case SL_WLAN_DISCONNECT_EVENT:
        {
            slWlanConnectAsyncResponse_t*  pEventData = NULL;

            CLR_STATUS_BIT(g_Status, STATUS_BIT_CONNECTION);
            CLR_STATUS_BIT(g_Status, STATUS_BIT_IP_ACQUIRED);

            pEventData = &pWlanEvent->EventData.STAandP2PModeDisconnected;

            /* If the user has initiated 'Disconnect' request, 'reason_code' is SL_USER_INITIATED_DISCONNECTION */
            if(SL_USER_INITIATED_DISCONNECTION == pEventData->reason_code)
            {
                CLI_Write(" Device disconnected from the AP on application's request \n\r");
            }
            else
            {
                CLI_Write(" Device disconnected from the AP on an ERROR..!! \n\r");
            }
        }
        break;

        default:
        {
            CLI_Write(" [WLAN EVENT] Unexpected event \n\r");
        }
        break;
    }
}

/*!
    \brief This function handles events for IP address acquisition via DHCP
           indication

    \param[in]      pNetAppEvent is the event passed to the handler

    \return         None

    \note

    \warning
*/
void SimpleLinkNetAppEventHandler(SlNetAppEvent_t *pNetAppEvent)
{
    if(pNetAppEvent == NULL)
        CLI_Write(" [NETAPP EVENT] NULL Pointer Error \n\r");
 
    switch(pNetAppEvent->Event)
    {
        case SL_NETAPP_IPV4_IPACQUIRED_EVENT:
        {
            SET_STATUS_BIT(g_Status, STATUS_BIT_IP_ACQUIRED);

            /*
             * Information about the connected AP's IP, gateway, DNS etc
             * will be available in 'SlIpV4AcquiredAsync_t' - Applications
             * can use it if required
             *
             * SlIpV4AcquiredAsync_t *pEventData = NULL;
             * pEventData = &pNetAppEvent->EventData.ipAcquiredV4;
             * <gateway_ip> = pEventData->gateway;
             *
             */
        }
        break;

        default:
        {
            CLI_Write(" [NETAPP EVENT] Unexpected event \n\r");
        }
        break;
    }
}

/*!
    \brief This function handles callback for the HTTP server events

    \param[in]      pHttpEvent - Contains the relevant event information
    \param[in]      pHttpResponse - Should be filled by the user with the
                    relevant response information

    \return         None

    \note

    \warning
*/
void SimpleLinkHttpServerCallback(SlHttpServerEvent_t *pHttpEvent,
                                  SlHttpServerResponse_t *pHttpResponse)
{
    /*
     * This application doesn't work with HTTP server - Hence these
     * events are not handled here
     */
    CLI_Write(" [HTTP EVENT] Unexpected event \n\r");
}

/*!
    \brief This function handles general error events indication

    \param[in]      pDevEvent is the event passed to the handler

    \return         None
*/
void SimpleLinkGeneralEventHandler(SlDeviceEvent_t *pDevEvent)
{
    /*
     * Most of the general errors are not FATAL are are to be handled
     * appropriately by the application
     */
    CLI_Write(" [GENERAL EVENT] \n\r");
}

/*!
    \brief This function handles socket events indication

    \param[in]      pSock is the event passed to the handler

    \return         None
*/
void SimpleLinkSockEventHandler(SlSockEvent_t *pSock)
{
    if(pSock == NULL)
        CLI_Write(" [SOCK EVENT] NULL Pointer Error \n\r");
    
    switch( pSock->Event )
    {
        case SL_SOCKET_TX_FAILED_EVENT:
        {
            /*
            * TX Failed
            *
            * Information about the socket descriptor and status will be
            * available in 'SlSockEventData_t' - Applications can use it if
            * required
            *
            * SlSockEventData_u *pEventData = NULL;
            * pEventData = & pSock->socketAsyncEvent;
            */
            switch( pSock->socketAsyncEvent.SockTxFailData.status)
            {
                case SL_ECLOSE:
                    CLI_Write(" [SOCK EVENT] Close socket operation failed to transmit all queued packets\n\r");
                break;


                default:
                    CLI_Write(" [SOCK EVENT] Unexpected event \n\r");
                break;
            }
        }
        break;

        default:
            CLI_Write(" [SOCK EVENT] Unexpected event \n\r");
        break;
    }
}
/*
 * ASYNCHRONOUS EVENT HANDLERS -- End
 */


/*
 * Application's entry point
 */

char *lookForTime(char *string, int *targetHour, int *targetMin) { // will return ptr to after TIME

    if (string == NULL) {
        return NULL;
    }

    char *ret = strstr(string, "Time:");

    if (ret == NULL) {
        return NULL;
    }

    char *timeLoc = ret + TIME_OFFSET; // Now at start of time value - this position can be hard-coded.
    char timeValue[MAX_TIME_STRING_LENGTH];
    memcpy(timeValue, timeLoc, MAX_TIME_STRING_LENGTH); // Copy time value into new buffer
    timeValue[MAX_TIME_STRING_LENGTH-1] = '\0'; // null terminate buffer
    sscanf(timeValue, "%d:%d", targetHour, targetMin); // convert into integers.
    return timeLoc;
}

char *lookForMass(char *string, int *targetMass) {

    if (string == NULL) {
        return NULL;
    }

    char *ret = strstr(string, "Amount:");

    if (ret == NULL) {
        return NULL;
    }

    char *amountLoc = ret + MASS_OFFSET; // Now at start of mass value - this position can be hard-coded.
    char mass[MAX_MASS_STRING_LENGTH];

    int i = 0;
    while(isdigit(*amountLoc)) { // loop to copy digit characters into msss array
        mass[i] = *amountLoc;
        i++;
        amountLoc++;
    }
    mass[i] = '\0';

    sscanf(mass, "%d", targetMass); // convert into integer.
    return amountLoc;
}

char *lookForSysTime(char *string, int *sysHour, int *sysMin) { // will return ptr to after "Date:"

    if (string == NULL) {
        return NULL;
    }

    char *ret = strstr(string, "Date:");

    if (ret == NULL) {
        return NULL;
    }

    char *sysTimeLoc = ret + SYS_TIME_OFFSET; // Now at start of time value - this position can be hard-coded.
    char sysTimeValue[MAX_TIME_STRING_LENGTH];
    memcpy(sysTimeValue, sysTimeLoc, MAX_TIME_STRING_LENGTH); // Copy time value into new buffer
    sysTimeValue[MAX_TIME_STRING_LENGTH-1] = '\0'; // null terminate buffer
    sscanf(sysTimeValue, "%d:%d", sysHour, sysMin); // convert into integers.
    *sysHour = (*sysHour + GMT_TO_AEST) % HOURS_IN_DAY;
    return sysTimeLoc;
}


void initPWM(void) {
	WDTCTL  = WDTPW + WDTHOLD;     // Kill watchdog timer
	TA0CCTL1 = OUTMOD_0;            // Setting capture/compare to output mode for initialisation
	TA0CTL   = TASSEL_2 + MC_1;     // Timer control register: source -> SMCLK, mode -> Up mode; counts from zero to value of TA0CCR0.
    TA0CTL = (TA0CTL | ID0 |ID1);
  	TA0CCR0  = PWM_FREQUENCY-1;        // PWM Period, TA0CTL will count up to this and reset.
	P1DIR   |= BIT2;               // P1.2 = output
	P1SEL   |= BIT2;               // P1.2 = TA1 output
}

void openServo(void) {
	TA0CCTL1 = OUTMOD_3;
    TA0CCR1 = OPEN_SERVO;

}

void closeServo(void) {
	TA0CCTL1 = OUTMOD_7;
    TA0CCR1 = CLOSE_SERVO;
}

void servoPWM (void) {


	closeServo();
    __delay_cycles(100000000);
	openServo();
    __delay_cycles(100000000);
}

double feedMode(int mass) {

	if (mass > get_Force()) {
		CLI_Write("FEEDING BEGINS\n\r");
	}
	while(mass > get_Force()) {
		openServo();
	}
	closeServo();
	CLI_Write("FEEDING DONE\n\r");

	return get_Force();

}

int main(int argc, char** argv)
{
    _i32 retVal = -1;
    int mass;
    double currentMass;
    int presentHour;
    int presentMin;
    int lastMin;
    retVal = initializeAppVariables();
    ASSERT_ON_ERROR(retVal);

    /* Stop WDT and initialize the system-clock of the MCU */
    stopWDT();
    initClk();

    /* Configure command line interface */
    CLI_Configure();
    init_ADC();
	initPWM();


	retVal = configureSimpleLinkToDefaultState();
	if(retVal < 0)
	{
		if (DEVICE_NOT_IN_STATION_MODE == retVal)
			CLI_Write(" Failed to configure the device in its default state \n\r");

		LOOP_FOREVER();
	}

	CLI_Write(" Device is configured in default state \n\r");

	retVal = sl_Start(0, 0, 0);
	if ((retVal < 0) ||
		(ROLE_STA != retVal) )
	{
		CLI_Write(" Failed to start the device \n\r");
		LOOP_FOREVER();
	}

	CLI_Write(" Device started as STATION \n\r");

	/* Connecting to WLAN AP */
	retVal = establishConnectionWithAP();
	if(retVal < 0)
	{
		CLI_Write(" Failed to establish connection w/ an AP \n\r");
		LOOP_FOREVER();
	}

	CLI_Write(" Connection established w/ AP and IP is acquired \n\r");


    // want to loop this bit

    while(1) {

		displayBanner();


		/*
		 * Assumption is that the device is configured in station mode already
		 * and it is in its default state
		 */
		timeWrapper(&presentHour, &presentMin);
		retVal = getWeather(&mass); // checks if feed time
		if(retVal == NO || lastMin == presentMin) { // not feed time
			CLI_Write(" Looks like those clowns in congress did it again\n\r");
		} else { // is feed time
    		CLI_Write("HOT DOG WE HAVE A WEINER\n\r");
    		currentMass = feedMode(mass);

		}
		// chedk if pet has eaten
		double diff = currentMass - get_Force();
		currentMass = get_Force();
		if(diff > 100) { // insert fudge factor so that we don't do stupid stuff
			CLI_Write("Pet has eaten\n\r");

    		putWrapper(presentHour, presentMin, diff, currentMass);

		}
		// check if empty


		//putWrapper(presentHour, presentMin, diff, currentMass); // test for now
		//servoPWM();


//	    __delay_cycles(200000000); // delay to prevent us from feeding multiple times per min.
	    lastMin = presentMin;
    }
	int otherRetVal = disconnectFromAP();
	if(otherRetVal < 0)
	{
		CLI_Write(" Failed to disconnect from AP \n\r");
		LOOP_FOREVER();
	}

    return 0;
}

void putWrapper(int timeHour, int timeMin, double amount, double left) {

    _i32 retVal = -1;

    pal_Strcpy((char *)g_AppData.HostName, WEATHER_SERVER);

    retVal = getHostIP();
    if(retVal < 0)
    {
        CLI_Write((_u8 *)" Unable to reach Host\n\r\n\r");
        ASSERT_ON_ERROR(retVal);
    }

    g_AppData.SockID = createConnection();
    ASSERT_ON_ERROR(g_AppData.SockID);

    pal_Memset(g_AppData.CityName, 0x00, sizeof(g_AppData.CityName));
    pal_Memcpy(g_AppData.CityName, CITY_NAME, pal_Strlen(CITY_NAME));
    g_AppData.CityName[pal_Strlen(CITY_NAME)] = '\0';

    putData(timeHour, timeMin, amount, left);

    retVal = sl_Close(g_AppData.SockID);
    ASSERT_ON_ERROR(retVal);
}

void putData(int timeHour, int timeMin, double amount, double left) {
	    _u8* p_bufLocation = NULL;
	    _i32 retVal = -1;

	    char hourStr[3];
	    char minStr[3];
	    char foodStr[4];
	    char leftStr[4];

	    sprintf(hourStr, "%d", timeHour);
	    sprintf(minStr, "%d", timeMin);
	    sprintf(foodStr, "%d", (int)amount);
	    sprintf(leftStr, "%d", (int)left);

	    /* Puts together the HTTP GET string. */
	    p_bufLocation = g_AppData.SendBuff;
	    pal_Strcpy(p_bufLocation, PUT_FIRST_BUF);
	    p_bufLocation += pal_Strlen(PUT_FIRST_BUF);

	    pal_Strcpy(p_bufLocation, PUT_BUF_TIME);
	    p_bufLocation += pal_Strlen(PUT_BUF_TIME);

	    pal_Strcpy(p_bufLocation, hourStr);
	    p_bufLocation += pal_Strlen(hourStr);

	    pal_Strcpy(p_bufLocation, PUT_BUF_SPACE);
	    p_bufLocation += pal_Strlen(PUT_BUF_SPACE);

	    pal_Strcpy(p_bufLocation, minStr);
	    p_bufLocation += pal_Strlen(minStr);

	    pal_Strcpy(p_bufLocation, PUT_BUF_FOOD);
	    p_bufLocation += pal_Strlen(PUT_BUF_FOOD);

	    pal_Strcpy(p_bufLocation, foodStr);
	    p_bufLocation += pal_Strlen(foodStr);

	    pal_Strcpy(p_bufLocation, PUT_BUF_LEFT);
	    p_bufLocation += pal_Strlen(PUT_BUF_LEFT);

	    pal_Strcpy(p_bufLocation, leftStr);
	    p_bufLocation += pal_Strlen(leftStr);

	    pal_Strcpy(p_bufLocation, PUT_SEC_BUF);

	    CLI_Write((_u8 *)g_AppData.SendBuff); // print out whole buffer

	    retVal = sl_Send(g_AppData.SockID, g_AppData.SendBuff, pal_Strlen(g_AppData.SendBuff), 0);
	    if(retVal != pal_Strlen(g_AppData.SendBuff))
	        ASSERT_ON_ERROR(HTTP_SEND_ERROR);

	    retVal = sl_Recv(g_AppData.SockID, &g_AppData.Recvbuff[0], MAX_SEND_RCV_SIZE, 0);
	    if(retVal <= 0) {
	        ASSERT_ON_ERROR(HTTP_RECV_ERROR);
	    }
	    g_AppData.Recvbuff[pal_Strlen(g_AppData.Recvbuff)] = '\0';

	    CLI_Write((_u8 *)g_AppData.Recvbuff); // print out whole buffer

	    return;
}

void timeWrapper(int *presentHour, int *presentMin) {
    _i32 retVal = -1;

    pal_Strcpy((char *)g_AppData.HostName, WEATHER_SERVER);

    retVal = getHostIP();
    if(retVal < 0)
    {
        CLI_Write((_u8 *)" Unable to reach Host\n\r\n\r");
        ASSERT_ON_ERROR(retVal);
    }

    g_AppData.SockID = createConnection();
    ASSERT_ON_ERROR(g_AppData.SockID);

    pal_Memset(g_AppData.CityName, 0x00, sizeof(g_AppData.CityName));
    pal_Memcpy(g_AppData.CityName, CITY_NAME, pal_Strlen(CITY_NAME));
    g_AppData.CityName[pal_Strlen(CITY_NAME)] = '\0';

    getTime(presentHour, presentMin);

    retVal = sl_Close(g_AppData.SockID);
    ASSERT_ON_ERROR(retVal);
}

int getTime(int *presentHour, int *presentMin){
    _u8* p_bufLocation = NULL;
    _i32 retVal = -1;

    pal_Memset(g_AppData.Recvbuff, 0, sizeof(g_AppData.Recvbuff));

    /* Puts together the HTTP GET string. */
    p_bufLocation = g_AppData.SendBuff;
    pal_Strcpy(p_bufLocation, PREFIX_BUFFER);

    p_bufLocation += pal_Strlen(PREFIX_BUFFER);
/*    pal_Strcpy(p_bufLocation, g_AppData.CityName);

    p_bufLocation += pal_Strlen(g_AppData.CityName);*/
    pal_Strcpy(p_bufLocation, POST_BUFFER);

    p_bufLocation += pal_Strlen(POST_BUFFER);
    pal_Strcpy(p_bufLocation, POST_BUFFER2);

    CLI_Write((_u8 *)g_AppData.SendBuff); // print out whole buffer

    /* Send the HTTP GET string to the open TCP/IP socket. */
    retVal = sl_Send(g_AppData.SockID, g_AppData.SendBuff, pal_Strlen(g_AppData.SendBuff), 0);
    if(retVal != pal_Strlen(g_AppData.SendBuff))
        ASSERT_ON_ERROR(HTTP_SEND_ERROR);

    /* Receive response */
    // as it stands we have a few problems with what we're doing.
    	// the received string is too long
    	// it also doesn't adhere to the format required.
    retVal = sl_Recv(g_AppData.SockID, &g_AppData.Recvbuff[0], MAX_SEND_RCV_SIZE, 0);
    if(retVal <= 0) {
        ASSERT_ON_ERROR(HTTP_RECV_ERROR);
    }
    g_AppData.Recvbuff[pal_Strlen(g_AppData.Recvbuff)] = '\0';

    lookForSysTime(g_AppData.Recvbuff, presentHour, presentMin);

    return SUCCESS;

}

/*!
    \brief This function Obtains the required data from the server

    \param[in]      none

    \return         0 on success, -ve otherwise

    \note

    \warning
*/
static _i32 getData(int *amount) // supposed to check if present time is a feed time
{
	_u8* p_bufLocation = NULL;
    _i32 retVal = -1;

    pal_Memset(g_AppData.Recvbuff, 0, sizeof(g_AppData.Recvbuff));

    /* Puts together the HTTP GET string. */
    p_bufLocation = g_AppData.SendBuff;
    pal_Strcpy(p_bufLocation, PREFIX_BUFFER);

    p_bufLocation += pal_Strlen(PREFIX_BUFFER);
/*    pal_Strcpy(p_bufLocation, g_AppData.CityName);

    p_bufLocation += pal_Strlen(g_AppData.CityName);*/
    pal_Strcpy(p_bufLocation, POST_BUFFER);

    p_bufLocation += pal_Strlen(POST_BUFFER);
    pal_Strcpy(p_bufLocation, POST_BUFFER2);

    CLI_Write((_u8 *)g_AppData.SendBuff); // print out whole buffer

    /* Send the HTTP GET string to the open TCP/IP socket. */
    retVal = sl_Send(g_AppData.SockID, g_AppData.SendBuff, pal_Strlen(g_AppData.SendBuff), 0);
    if(retVal != pal_Strlen(g_AppData.SendBuff))
        ASSERT_ON_ERROR(HTTP_SEND_ERROR);

    /* Receive response */
    // as it stands we have a few problems with what we're doing.
    	// the received string is too long
    	// it also doesn't adhere to the format required.
    retVal = sl_Recv(g_AppData.SockID, &g_AppData.Recvbuff[0], MAX_SEND_RCV_SIZE, 0);
    if(retVal <= 0) {
        ASSERT_ON_ERROR(HTTP_RECV_ERROR);
    }
    g_AppData.Recvbuff[pal_Strlen(g_AppData.Recvbuff)] = '\0';

    CLI_Write((_u8 *)g_AppData.Recvbuff); // print out whole buffer

    // Interpretting buffer

        char *ret;

        int targetHour[MAX_FEED_TIMES];
        int targetMin[MAX_FEED_TIMES];
        int targetMass[MAX_FEED_TIMES];
        int presentHour;
        int presentMin;
        char poo[128];
        int feedTimes;

        ret = lookForSysTime(g_AppData.Recvbuff, &presentHour, &presentMin);

        sprintf(poo ,"HOUR: %d MINUTE: %d \n\r", presentHour, presentMin);
        CLI_Write((_u8 *)poo);
        //ret = lookForMass(ret, targetMass);
        //sprintf(poo, "%d\n", targetMass[0]);
        //CLI_Write((_u8 *)poo);
        for (feedTimes = 0; feedTimes < MAX_FEED_TIMES; feedTimes++) {
            ret = lookForTime(ret, targetHour+feedTimes, targetMin+feedTimes); // muh pointer arithmetic
            if(ret == NULL) {
                break;
            }
            ret = lookForMass(ret, targetMass+feedTimes);
            sprintf(poo, "TIME[%d] = %d:%d\n\r", feedTimes, targetHour[feedTimes], targetMin[feedTimes]);
            CLI_Write((_u8 *)poo);
            sprintf(poo, "MASS[%d] = %d\n\r", feedTimes, targetMass[feedTimes]);
            CLI_Write((_u8 *)poo);
            if(ret == NULL) {
                break;
            }
        }

        sprintf(poo, "feedTimes %d\n\r", feedTimes); // this is the number of times the pets are being fed
        CLI_Write((_u8 *)poo);
        int j;
        for(j = 0; j < feedTimes ; j++) {
        	if(presentHour == targetHour[j] && presentMin == targetMin[j]) {
        		*amount = targetMass[j];
        		return YES;
        	}
        }

    return NO;
}

/*!
    \brief Create TCP connection with openweathermap.org

    \param[in]      none

    \return         Socket descriptor for success otherwise negative

    \warning
*/
static _i32 createConnection()
{
    SlSockAddrIn_t  Addr;

    _i16 sd = 0;
    _i16 AddrSize = 0;
    _i32 ret_val = 0;

    Addr.sin_family = SL_AF_INET;
    Addr.sin_port = sl_Htons(80);

    /* Change the DestinationIP endianity, to big endian */
    Addr.sin_addr.s_addr = sl_Htonl(g_AppData.DestinationIP);

    AddrSize = sizeof(SlSockAddrIn_t);

    sd = sl_Socket(SL_AF_INET,SL_SOCK_STREAM, 0);
    if( sd < 0 )
    {
        CLI_Write((_u8 *)" Error creating socket\n\r\n\r");
        ASSERT_ON_ERROR(sd);
    }

    ret_val = sl_Connect(sd, ( SlSockAddr_t *)&Addr, AddrSize);
    if( ret_val < 0 )
    {
        /* error */
        CLI_Write((_u8 *)" Error connecting to server\n\r\n\r");
        ASSERT_ON_ERROR(ret_val);
    }

    return sd;
}

/*!
    \brief This function obtains the server IP address

    \param[in]      none

    \return         zero for success and -1 for error

    \warning
*/
static _i32 getHostIP()
{
    _i32 status = -1;

    status = sl_NetAppDnsGetHostByName((_i8 *)g_AppData.HostName,
                                       pal_Strlen(g_AppData.HostName),
                                       &g_AppData.DestinationIP, SL_AF_INET);
    ASSERT_ON_ERROR(status);

    return SUCCESS;
}

/*!
    \brief Get the Weather from server

    \param[in]      none

    \return         zero for success and -1 for error

    \warning
*/
static _i32 getWeather(int *mass)
{
    _i32 retVal = -1;

    pal_Strcpy((char *)g_AppData.HostName, WEATHER_SERVER);

    retVal = getHostIP();
    if(retVal < 0)
    {
        CLI_Write((_u8 *)" Unable to reach Host\n\r\n\r");
        ASSERT_ON_ERROR(retVal);
    }

    g_AppData.SockID = createConnection();
    ASSERT_ON_ERROR(g_AppData.SockID);

    pal_Memset(g_AppData.CityName, 0x00, sizeof(g_AppData.CityName));
    pal_Memcpy(g_AppData.CityName, CITY_NAME, pal_Strlen(CITY_NAME));
    g_AppData.CityName[pal_Strlen(CITY_NAME)] = '\0';

    int amount;
    int yesOrNo;
    yesOrNo = getData(&amount);

    *mass = amount;

    retVal = sl_Close(g_AppData.SockID);
    ASSERT_ON_ERROR(retVal);

    return yesOrNo;
}

/*!
    \brief This function configure the SimpleLink device in its default state. It:
           - Sets the mode to STATION
           - Configures connection policy to Auto and AutoSmartConfig
           - Deletes all the stored profiles
           - Enables DHCP
           - Disables Scan policy
           - Sets Tx power to maximum
           - Sets power policy to normal
           - Unregisters mDNS services
           - Remove all filters

    \param[in]      none

    \return         On success, zero is returned. On error, negative is returned
*/
static _i32 configureSimpleLinkToDefaultState()
{
    SlVersionFull   ver = {0};
    _WlanRxFilterOperationCommandBuff_t  RxFilterIdMask = {0};

    _u8           val = 1;
    _u8           configOpt = 0;
    _u8           configLen = 0;
    _u8           power = 0;

    _i32          retVal = -1;
    _i32          mode = -1;

    mode = sl_Start(0, 0, 0);
    ASSERT_ON_ERROR(mode);

    /* If the device is not in station-mode, try configuring it in station-mode */
    if (ROLE_STA != mode)
    {
        if (ROLE_AP == mode)
        {
            /* If the device is in AP mode, we need to wait for this event before doing anything */
            while(!IS_IP_ACQUIRED(g_Status)) { _SlNonOsMainLoopTask(); }
        }

        /* Switch to STA role and restart */
        retVal = sl_WlanSetMode(ROLE_STA);
        ASSERT_ON_ERROR(retVal);

        retVal = sl_Stop(SL_STOP_TIMEOUT);
        ASSERT_ON_ERROR(retVal);

        retVal = sl_Start(0, 0, 0);
        ASSERT_ON_ERROR(retVal);

        /* Check if the device is in station again */
        if (ROLE_STA != retVal)
        {
            /* We don't want to proceed if the device is not coming up in station-mode */
            ASSERT_ON_ERROR(DEVICE_NOT_IN_STATION_MODE);
        }
    }

    /* Get the device's version-information */
    configOpt = SL_DEVICE_GENERAL_VERSION;
    configLen = sizeof(ver);
    retVal = sl_DevGet(SL_DEVICE_GENERAL_CONFIGURATION, &configOpt, &configLen, (_u8 *)(&ver));
    ASSERT_ON_ERROR(retVal);

    /* Set connection policy to Auto + SmartConfig (Device's default connection policy) */
    retVal = sl_WlanPolicySet(SL_POLICY_CONNECTION, SL_CONNECTION_POLICY(1, 0, 0, 0, 1), NULL, 0);
    ASSERT_ON_ERROR(retVal);

    /* Remove all profiles */
    retVal = sl_WlanProfileDel(0xFF);
    ASSERT_ON_ERROR(retVal);

    /*
     * Device in station-mode. Disconnect previous connection if any
     * The function returns 0 if 'Disconnected done', negative number if already disconnected
     * Wait for 'disconnection' event if 0 is returned, Ignore other return-codes
     */
    retVal = sl_WlanDisconnect();
    if(0 == retVal)
    {
        /* Wait */
        while(IS_CONNECTED(g_Status)) { _SlNonOsMainLoopTask(); }
    }

    /* Enable DHCP client*/
    retVal = sl_NetCfgSet(SL_IPV4_STA_P2P_CL_DHCP_ENABLE,1,1,&val);
    ASSERT_ON_ERROR(retVal);

    /* Disable scan */
    configOpt = SL_SCAN_POLICY(0);
    retVal = sl_WlanPolicySet(SL_POLICY_SCAN , configOpt, NULL, 0);
    ASSERT_ON_ERROR(retVal);

    /* Set Tx power level for station mode
       Number between 0-15, as dB offset from max power - 0 will set maximum power */
    power = 0;
    retVal = sl_WlanSet(SL_WLAN_CFG_GENERAL_PARAM_ID, WLAN_GENERAL_PARAM_OPT_STA_TX_POWER, 1, (_u8 *)&power);
    ASSERT_ON_ERROR(retVal);

    /* Set PM policy to normal */
    retVal = sl_WlanPolicySet(SL_POLICY_PM , SL_NORMAL_POLICY, NULL, 0);
    ASSERT_ON_ERROR(retVal);

    /* Unregister mDNS services */
    retVal = sl_NetAppMDNSUnRegisterService(0, 0);
    ASSERT_ON_ERROR(retVal);

    /* Remove  all 64 filters (8*8) */
    pal_Memset(RxFilterIdMask.FilterIdMask, 0xFF, 8);
    retVal = sl_WlanRxFilterSet(SL_REMOVE_RX_FILTER, (_u8 *)&RxFilterIdMask,
                       sizeof(_WlanRxFilterOperationCommandBuff_t));
    ASSERT_ON_ERROR(retVal);

    retVal = sl_Stop(SL_STOP_TIMEOUT);
    ASSERT_ON_ERROR(retVal);

    retVal = initializeAppVariables();
    ASSERT_ON_ERROR(retVal);

    return retVal; /* Success */
}

/*!
    \brief Connecting to a WLAN Access point

    This function connects to the required AP (SSID_NAME).
    The function will return once we are connected and have acquired IP address

    \param[in]  None

    \return     0 on success, negative error-code on error

    \note

    \warning    If the WLAN connection fails or we don't acquire an IP address,
                We will be stuck in this function forever.
*/
static _i32 establishConnectionWithAP()
{
    SlSecParams_t secParams = {0};
    _i32 retVal = 0;

    secParams.Key = PASSKEY;
    secParams.KeyLen = PASSKEY_LEN;
    secParams.Type = SEC_TYPE;

    retVal = sl_WlanConnect(SSID_NAME, pal_Strlen(SSID_NAME), 0, &secParams, 0);
    ASSERT_ON_ERROR(retVal);

    /* Wait */
    while((!IS_CONNECTED(g_Status)) || (!IS_IP_ACQUIRED(g_Status))) { _SlNonOsMainLoopTask(); }

    return SUCCESS;
}

/*!
    \brief Disconnecting from a WLAN Access point

    This function disconnects from the connected AP

    \param[in]      None

    \return         none

    \note

    \warning        If the WLAN disconnection fails, we will be stuck in this function forever.
*/
static _i32 disconnectFromAP()
{
    _i32 retVal = -1;

    /*
     * The function returns 0 if 'Disconnected done', negative number if already disconnected
     * Wait for 'disconnection' event if 0 is returned, Ignore other return-codes
     */
    retVal = sl_WlanDisconnect();
    if(0 == retVal)
    {
        /* Wait */
        while(IS_CONNECTED(g_Status)) { _SlNonOsMainLoopTask(); }
    }

    return SUCCESS;
}

/*!
    \brief This function initializes the application variables

    \param[in]  None

    \return     0 on success, negative error-code on error
*/
static _i32 initializeAppVariables()
{
    g_Status = 0;
    pal_Memset(&g_AppData, 0, sizeof(g_AppData));

    return SUCCESS;
}

/*!
    \brief This function displays the application's banner

    \param      None

    \return     None
*/
static void displayBanner()
{
    CLI_Write("\n\r\n\r");
    CLI_Write(" Get weather application - Version ");
    CLI_Write(APPLICATION_VERSION);
    CLI_Write("\n\r*******************************************************************************\n\r");
}
