################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lnk_msp430f5529.cmd 

C_SRCS += \
../adc10_a.c \
../adc12_a.c \
../aes.c \
../battbak.c \
../comp_b.c \
../crc.c \
../ctsd16.c \
../dac12_a.c \
../dma.c \
../eusci_a_spi.c \
../eusci_a_uart.c \
../eusci_b_i2c.c \
../eusci_b_spi.c \
../flashctl.c \
../gpio.c \
../lcd_b.c \
../ldopwr.c \
../main.c \
../mpy32.c \
../oa.c \
../pmap.c \
../pmm.c \
../ram.c \
../ref.c \
../rtc_a.c \
../rtc_b.c \
../rtc_c.c \
../sd24_b.c \
../sfr.c \
../sysctl.c \
../tec.c \
../timer_a.c \
../timer_b.c \
../timer_d.c \
../tlv.c \
../ucs.c \
../usci_a_spi.c \
../usci_a_uart.c \
../usci_b_i2c.c 

OBJS += \
./adc10_a.obj \
./adc12_a.obj \
./aes.obj \
./battbak.obj \
./comp_b.obj \
./crc.obj \
./ctsd16.obj \
./dac12_a.obj \
./dma.obj \
./eusci_a_spi.obj \
./eusci_a_uart.obj \
./eusci_b_i2c.obj \
./eusci_b_spi.obj \
./flashctl.obj \
./gpio.obj \
./lcd_b.obj \
./ldopwr.obj \
./main.obj \
./mpy32.obj \
./oa.obj \
./pmap.obj \
./pmm.obj \
./ram.obj \
./ref.obj \
./rtc_a.obj \
./rtc_b.obj \
./rtc_c.obj \
./sd24_b.obj \
./sfr.obj \
./sysctl.obj \
./tec.obj \
./timer_a.obj \
./timer_b.obj \
./timer_d.obj \
./tlv.obj \
./ucs.obj \
./usci_a_spi.obj \
./usci_a_uart.obj \
./usci_b_i2c.obj 

C_DEPS += \
./adc10_a.pp \
./adc12_a.pp \
./aes.pp \
./battbak.pp \
./comp_b.pp \
./crc.pp \
./ctsd16.pp \
./dac12_a.pp \
./dma.pp \
./eusci_a_spi.pp \
./eusci_a_uart.pp \
./eusci_b_i2c.pp \
./eusci_b_spi.pp \
./flashctl.pp \
./gpio.pp \
./lcd_b.pp \
./ldopwr.pp \
./main.pp \
./mpy32.pp \
./oa.pp \
./pmap.pp \
./pmm.pp \
./ram.pp \
./ref.pp \
./rtc_a.pp \
./rtc_b.pp \
./rtc_c.pp \
./sd24_b.pp \
./sfr.pp \
./sysctl.pp \
./tec.pp \
./timer_a.pp \
./timer_b.pp \
./timer_d.pp \
./tlv.pp \
./ucs.pp \
./usci_a_spi.pp \
./usci_a_uart.pp \
./usci_b_i2c.pp 

C_DEPS__QUOTED += \
"adc10_a.pp" \
"adc12_a.pp" \
"aes.pp" \
"battbak.pp" \
"comp_b.pp" \
"crc.pp" \
"ctsd16.pp" \
"dac12_a.pp" \
"dma.pp" \
"eusci_a_spi.pp" \
"eusci_a_uart.pp" \
"eusci_b_i2c.pp" \
"eusci_b_spi.pp" \
"flashctl.pp" \
"gpio.pp" \
"lcd_b.pp" \
"ldopwr.pp" \
"main.pp" \
"mpy32.pp" \
"oa.pp" \
"pmap.pp" \
"pmm.pp" \
"ram.pp" \
"ref.pp" \
"rtc_a.pp" \
"rtc_b.pp" \
"rtc_c.pp" \
"sd24_b.pp" \
"sfr.pp" \
"sysctl.pp" \
"tec.pp" \
"timer_a.pp" \
"timer_b.pp" \
"timer_d.pp" \
"tlv.pp" \
"ucs.pp" \
"usci_a_spi.pp" \
"usci_a_uart.pp" \
"usci_b_i2c.pp" 

OBJS__QUOTED += \
"adc10_a.obj" \
"adc12_a.obj" \
"aes.obj" \
"battbak.obj" \
"comp_b.obj" \
"crc.obj" \
"ctsd16.obj" \
"dac12_a.obj" \
"dma.obj" \
"eusci_a_spi.obj" \
"eusci_a_uart.obj" \
"eusci_b_i2c.obj" \
"eusci_b_spi.obj" \
"flashctl.obj" \
"gpio.obj" \
"lcd_b.obj" \
"ldopwr.obj" \
"main.obj" \
"mpy32.obj" \
"oa.obj" \
"pmap.obj" \
"pmm.obj" \
"ram.obj" \
"ref.obj" \
"rtc_a.obj" \
"rtc_b.obj" \
"rtc_c.obj" \
"sd24_b.obj" \
"sfr.obj" \
"sysctl.obj" \
"tec.obj" \
"timer_a.obj" \
"timer_b.obj" \
"timer_d.obj" \
"tlv.obj" \
"ucs.obj" \
"usci_a_spi.obj" \
"usci_a_uart.obj" \
"usci_b_i2c.obj" 

C_SRCS__QUOTED += \
"../adc10_a.c" \
"../adc12_a.c" \
"../aes.c" \
"../battbak.c" \
"../comp_b.c" \
"../crc.c" \
"../ctsd16.c" \
"../dac12_a.c" \
"../dma.c" \
"../eusci_a_spi.c" \
"../eusci_a_uart.c" \
"../eusci_b_i2c.c" \
"../eusci_b_spi.c" \
"../flashctl.c" \
"../gpio.c" \
"../lcd_b.c" \
"../ldopwr.c" \
"../main.c" \
"../mpy32.c" \
"../oa.c" \
"../pmap.c" \
"../pmm.c" \
"../ram.c" \
"../ref.c" \
"../rtc_a.c" \
"../rtc_b.c" \
"../rtc_c.c" \
"../sd24_b.c" \
"../sfr.c" \
"../sysctl.c" \
"../tec.c" \
"../timer_a.c" \
"../timer_b.c" \
"../timer_d.c" \
"../tlv.c" \
"../ucs.c" \
"../usci_a_spi.c" \
"../usci_a_uart.c" \
"../usci_b_i2c.c" 


