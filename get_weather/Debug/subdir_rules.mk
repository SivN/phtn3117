################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
adc10_a.obj: ../adc10_a.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="adc10_a.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

adc12_a.obj: ../adc12_a.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="adc12_a.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

aes.obj: ../aes.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="aes.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

battbak.obj: ../battbak.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="battbak.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

comp_b.obj: ../comp_b.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="comp_b.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

crc.obj: ../crc.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="crc.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

ctsd16.obj: ../ctsd16.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="ctsd16.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

dac12_a.obj: ../dac12_a.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="dac12_a.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

dma.obj: ../dma.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="dma.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

eusci_a_spi.obj: ../eusci_a_spi.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="eusci_a_spi.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

eusci_a_uart.obj: ../eusci_a_uart.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="eusci_a_uart.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

eusci_b_i2c.obj: ../eusci_b_i2c.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="eusci_b_i2c.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

eusci_b_spi.obj: ../eusci_b_spi.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="eusci_b_spi.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

flashctl.obj: ../flashctl.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="flashctl.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

gpio.obj: ../gpio.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="gpio.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lcd_b.obj: ../lcd_b.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="lcd_b.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

ldopwr.obj: ../ldopwr.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="ldopwr.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

main.obj: ../main.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="main.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

mpy32.obj: ../mpy32.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="mpy32.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

oa.obj: ../oa.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="oa.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

pmap.obj: ../pmap.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="pmap.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

pmm.obj: ../pmm.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="pmm.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

ram.obj: ../ram.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="ram.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

ref.obj: ../ref.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="ref.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

rtc_a.obj: ../rtc_a.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="rtc_a.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

rtc_b.obj: ../rtc_b.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="rtc_b.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

rtc_c.obj: ../rtc_c.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="rtc_c.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

sd24_b.obj: ../sd24_b.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="sd24_b.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

sfr.obj: ../sfr.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="sfr.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

sysctl.obj: ../sysctl.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="sysctl.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

tec.obj: ../tec.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="tec.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

timer_a.obj: ../timer_a.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="timer_a.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

timer_b.obj: ../timer_b.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="timer_b.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

timer_d.obj: ../timer_d.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="timer_d.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

tlv.obj: ../tlv.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="tlv.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

ucs.obj: ../ucs.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="ucs.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

usci_a_spi.obj: ../usci_a_spi.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="usci_a_spi.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

usci_a_uart.obj: ../usci_a_uart.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="usci_a_uart.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

usci_b_i2c.obj: ../usci_b_i2c.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/bin/cl430" -vmspx --abi=coffabi --code_model=small -Ooff --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/examples/common" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/include" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/simplelink/source" --include_path="C:/ti/CC3100SDK_1.1.0/cc3100-sdk/platform/msp430f5529lp" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.5/include" -g --define=__CCS__ --define=__MSP430F5529__ --define=_USE_CLI_ --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="usci_b_i2c.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


